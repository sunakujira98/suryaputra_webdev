<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Suryaputra Bus Pariwisata - {{$bus->name}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('includes.meta')
    @include('includes.meta-pages')
    <!-- <link rel="manifest" href="site.webmanifest"> -->
    <link rel="shortcut icon" type="image/x-icon" href="img/logo4.png">
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/themify-icons.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/gijgo.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/slicknav.css">
    <link rel="stylesheet" href="{{ asset('plugins/lightbox/css/lightbox.css')}}">
    <link rel="stylesheet" href="{{ asset('racks/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('racks/css/custom.css')}}">
    <link rel="stylesheet" href="css/style.css">

</head>

<body>
    @include('includes.header')
    <!-- Banner-->
    <div class="site-blocks-cover overlay background-dynamic" data-aos="fade" id="home-section">
        <div class="banner-title">
            <h1 class="text color-responsive" data-aos="fade-up">Armada Kami</h1>
            <hr class="text-center hr-color">
        </div>
    </div>
    <img src=" {{url('img/block.PNG')}}" width="100%;" height="50px;">

    <div class="popular_places_area">
        <div class="container">
            @include('includes.flash-message')
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-4 col-md-4">
                            <div class="single_place">
                                <div class="thumb">
                                    <img src="{{'https://drive.google.com/uc?export=view&id='.$bus->image_url}}" alt="{{$bus->name}}">
                                </div>
                                <div class="place_info">
                                    <h3 style="font-family:Kanit script=latin rev=1; font-size: 20px; font-style: normal; font-variant: normal; font-weight: 600;color:#090109;">{{$bus->name}}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-8">
                            <div class="destination_info">
                                <h3 style="font-family:Kanit script=latin rev=1; font-size: 20px; font-style: normal; font-variant: normal; font-weight: 600;color:#090109;">{{$bus->name}}</h3>
                                <p class="fontcb">{!! $bus->number_of_seats_description !!}</p>
                                <div class="single_destination mt-3">
                                    <h4 style="font-family:Kanit script=latin rev=1; font-size: 20px; font-style: normal; font-variant: normal; font-weight: 600;color:#090109;">Fasilitas</h4>
                                    <p class="fontcb"> {!! $bus->facility !!}</p>
                                </div>
                                <div class="single_destination mt-3">
                                    <h4 style="font-family:Kanit script=latin rev=1; font-size: 20px; font-style: normal; font-variant: normal; font-weight: 600;color:#090109;">Kapasitas Tempat Duduk</h4>
                                    <p class="fontcb"> {!! $bus->number_of_seats_description !!} </p>
                                </div>
                                <div class="single_destination mt-3">
                                    <h4 style="font-family:Kanit script=latin rev=1; font-size: 20px; font-style: normal; font-variant: normal; font-weight: 600;color:#090109;">Design Bus</h4>
                                    <p class="fontcb"> {!! $bus->design!!}</p>
                                </div>
                            </div>
                            <hr>
                            <h4 style="font-family:Kanit script=latin rev=1; font-size: 20px; font-style: normal; font-variant: normal; font-weight: 600;color:#090109;">Galeri</h4>
                            <div class="row">
                                <div class="col-md-3">
                                    <a href="{{'https://drive.google.com/uc?export=view&id='.$bus->image_url}}" data-lightbox="image-{{$bus->id}}">
                                        <img src="{{'https://drive.google.com/uc?export=view&id='.$bus->image_url}}" class="img-fluid mb-2" alt="Bus pariwisata terbaik di bandung">
                                    </a>
                                </div>
                                @foreach($busDetail as $busDetails)
                                <div class="col-md-3">
                                    <a href="{{'https://drive.google.com/uc?export=view&id='.$busDetails->image_url}}" data-lightbox="image-{{$bus->id}}">
                                        <img src="{{'https://drive.google.com/uc?export=view&id='.$busDetails->image_url}}" class="img-fluid mb-2" alt="{{$busDetails->name}}">
                                    </a>
                                </div>
                                @endforeach
                            </div>
                            <hr>
                            <h4 style="font-family:Kanit script=latin rev=1; font-size: 20px; font-style: normal; font-variant: normal; font-weight: 600;color:#090109;">Tertarik Dengan Bus Ini ? </h4>
                            <div class="destination_info">
                                <a href="{{$pageConfiguration->whatsapp_api}}&text=Halo%20Suryaputra%21%20Mau%20nanya%20informasi%20lebih%20lanjut%20tentang%20{{$bus->name}}%20Nih" target="_blank" class="boxed-btn4" style="margin-bottom:10px;">Chat CS (WA)</a>
                                <button type="button" class="boxed-btn4" data-toggle="modal" data-target="#modal-book-armada"> Minta CS Hubungi Saya</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section ftco-degree-bg">
        <div class="container newsletter-section">
            <div class="newsletter-content">
                <div class="row justify-content-center" style="margin-bottom:20px">
                    <div class="col-md-7 text-center heading-section ftco-animate">
                        <h2 style="font-family: Arial; font-size: 20px; font-style: normal; font-variant: normal; line-height: 26.4px; color:#328953;">STAY UP TO DATE WITH OUR NEWSLETTER</h2>
                    </div>
                </div>
                <form action="{{ route('subscribe')}}" method="post">
                    <div class="row justify-content-center">
                        @csrf
                        <div class="col-md-6" style="text-align:right; padding:0px">
                            <input class="content-input" type="email" placeholder="Berlangganan Newsletter" name="email">
                        </div>
                        <div class="col-md-1" style="text-align:left; padding:0px">
                            <button class="btn btn-success">SEND</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>

    <div class="popular_destination_area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section_title text-center mb_70">
                        <h3 style="font-family:Kanit script=latin rev=1; font-style: normal; font-variant: normal; font-weight: 600;color:#090109;">Armada Bus Suryaputra</h3>
                        <p style="font-family: Kanit script=latin rev=1; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 20px; color:#090109">Armada bus pariwisata Suryaputra terdiri dari Microbus Elf, Medium Bus & Big Bus. Yang masing-masing berkapasitas tempat duduk 27 seat, 29 seat, dan 47 seat. Armada mewah kami di dukung oleh brand ternama, seperti Mercedes Benz yang telah lama terkenal dengan kenyamanan dan keamanannya, dan juga Mitsubishi, Isuzu Elf serta Toyota HIACE Commuter yang tangguh..</p>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($armada as $angkutan)
                @php
                $hypehenSentece = \Util::hypehenize($angkutan->name);
                @endphp
                <div class="col-lg-4 col-md-6">
                    <a href="{{ url('/busdetail-'.$angkutan->id.'-'.$hypehenSentece)}}">
                        <div class="single_destination">
                            <div class="thumb">
                                <img src="{{'https://drive.google.com/uc?export=view&id='.$angkutan->image_url}}" alt="{{$angkutan->name}}">
                            </div>
                            @php
                            $hypehenSentece = \Util::hypehenize($angkutan->name);
                            @endphp
                            <div class="content">
                                <p class="d-flex align-items-center" style="font-family:Kanit script=latin rev=1">{{$angkutan->name}} {{$angkutan->number_of_seats .' Seats'}} </p>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <a href="{{$pageConfiguration->whatsapp_api}}&text=Halo%20Suryaputra%21%20Mau%20nanya%20informasi%20lebih%20lanjut%20nih." class="float" target="_blank">
        <i class="fa fa-whatsapp my-float"></i>
    </a>

    @include('includes.footer')


    <!-- Modal -->
    <div class="modal fade custom_search_pop" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="serch_form">
                    <input type="text" placeholder="Search">
                    <button type="submit">search</button>
                </div>
            </div>
        </div>
    </div>
    <!-- link that opens popup -->


    <div class="modal fade" id="modal-book-armada">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Harap masukan form dibawah ini, kita akan hubungi anda lebih lanjut..</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('book')}}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="hidden" class="form-control" autocomplete="off" name="input_bus_name" value="{{$bus->name}}">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Email Anda</label>
                                    <input type="text" class="form-control" autocomplete="off" name="input_email" required placeholder="Harap masukkan email anda . . ">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>No HP</label>
                                    <input type="text" class="form-control" autocomplete="off" name="input_telp" required placeholder="Harap masukkan no telp anda . .">
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Chat Sekarang</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- JS here -->
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/ajax-form.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/scrollIt.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/jquery-ui.min.js"> </script>
    <script src="js/nice-select.min.js"></script>
    <script src="js/jquery.slicknav.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/range.js"></script>
    <!-- <script src="js/gijgo.min.js"></script> -->
    <script src="js/slick.min.js"></script>
    <script src="{{ asset('racks/js/navigation.js')}}"></script>



    <!--contact js-->
    <script src="js/contact.js"></script>
    <script src="js/jquery.ajaxchimp.min.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/mail-script.js"></script>

    <!-- Ekko Lightbox -->
    <script src="{{ asset('plugins/lightbox/js/lightbox.js')}}"></script>
    <script src="js/main.js"></script>
    <script>
        $('#datepicker').datepicker({
            iconsLibrary: 'fontawesome',
            icons: {
                rightIcon: '<span class="fa fa-caret-down"></span>'
            }
        });
    </script>
    </script>
    <script>
        $(function() {
            $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox({
                    alwaysShowClose: true
                });
            });
        })
    </script>

</body>

</html>