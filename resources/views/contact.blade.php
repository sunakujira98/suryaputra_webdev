<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Suryaputra Bus Pariwisata - Kontak</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('includes.meta')
    @include('includes.meta-pages')
    <!-- <link rel="manifest" href="site.webmanifest"> -->
    <link rel="shortcut icon" type="image/x-icon" href="img/logo4.png">
    <!-- Place favicon.ico in the root directory -->
    <!-- CSS here -->
    <link rel="stylesheet" href="{{ asset('fonts/kanitsemibold/stylesheet.css')}}">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/themify-icons.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/gijgo.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/slicknav.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="{{ asset('racks/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('racks/css/custom.css')}}">
</head>

<body>
    @include('includes.header')
    <!-- Banner-->
    <div class="site-blocks-cover overlay background-dynamic" data-aos="fade" id="home-section">
        <div class="banner-title">
            <h1 class="text color-responsive" data-aos="fade-up">Kontak Kami</h1>
            <hr class="text-center hr-color">
        </div>
    </div>
    <img src=" {{url('img/block.PNG')}}" width="100%;" height="50px;">

    <!-- ================ contact section start ================= -->
    <section class="contact-section">
        <div class="container">
            @include('includes.flash-message')
            <div class="row">
                <div class="col-12">
                    <h2 class="contact-title" style="font-family: Arial; font-size: 22px; font-style: normal; font-variant: normal; font-weight: 700;color:#328953;">Isi Form Untuk Mengontak Kami</h2>
                </div>
                <div class="col-lg-8">
                    <form class="form-contact contact_form" action="{{ route('contact')}}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <textarea class="form-control w-100" name="message" id="message" cols="30" rows="15" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Masukkan Pesan'" placeholder=" Enter Message" required></textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control valid" name="name" id="name" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nama Anda'" placeholder="Nama Anda .." required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control valid" name="email" id="email" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Anda'" placeholder="Email Anda" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <button type="submit" class="button button-contactForm boxed-btn">Send</button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-3 offset-lg-1">
                    <div class="media contact-info">
                        <span class="contact-info__icon"><i class="ti-home"></i></span>
                        <div class="media-body">
                            <h3>{{$pageConfiguration->address_line1}}</h3>
                            <p>{{$pageConfiguration->address_line2}}</p>
                        </div>
                    </div>
                    <div class="media contact-info">
                        <span class="contact-info__icon"><i class="ti-mobile"></i></span>
                        <div class="media-body">
                            <a href="{{$pageConfiguration->whatsapp_api}}&text=Halo%20Suryaputra%21%20Mau%20nanya%20informasi%20lebih%20lanjut%20nih.">
                                <h3>{{$pageConfiguration->whatsapp_number}}</h3>
                                <p>Senin - Jumat 08.00 - 18.00 , Sabtu 08.00 - 15.00</p>
                            </a>
                        </div>
                    </div>
                    <div class="media contact-info">
                        <span class="contact-info__icon"><i class="ti-email"></i></span>
                        <div class="media-body">
                            <h3>
                                <p><a href="mailto:{{$pageConfiguration->email_address}}">Klik disini untuk kirim email</p>
                                {{$pageConfiguration->email_address}}
                            </h3>
                            </a>
                        </div>
                    </div>
                    <div class="media contact-info">
                        <span class="contact-info__icon"><i class="ti-mobile"></i></span>
                        <div class="media-body">
                            <a href="tel:{{$pageConfiguration->telp_bandung}}">
                                <h3>{{$pageConfiguration->telp_bandung . ' (Bandung)'}}</h3>
                            </a>
                        </div>
                    </div>
                    <div class="media contact-info">
                        <span class="contact-info__icon"><i class="ti-mobile"></i></span>
                        <div class="media-body">
                            <a href="tel:{{$pageConfiguration->telp_jakarta}}">
                                <h3>{{$pageConfiguration->telp_jakarta. ' (Jakarta)'}}</h3>
                            </a>
                        </div>
                    </div>
                    <div class="media contact-info">
                        <span class="contact-info__icon"><i class="ti-mobile"></i></span>
                        <div class="media-body">
                            <a href="tel:{{$pageConfiguration->telp_cirebon}}">
                                <h3>{{$pageConfiguration->telp_cirebon . ' (Cirebon)'}}</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-none d-sm-block mb-5 pb-4">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15842.104521104968!2d107.5978346!3d-6.9470921!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x5423747e1152caf8!2sPT%20Suryaputra%20Adipradana!5e0!3m2!1sen!2sid!4v1595775810324!5m2!1sen!2sid" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <h2 style="font-family: Arial; font-size: 24px; font-style: normal; font-variant: normal; font-weight: 700;color:#328953;">
                                FAQ
                            </h2>
                            <div class="destination_info">
                                <h3 style="font-family: Arial; font-size: 20px; font-style: normal; font-variant: normal; font-weight: 700;color:#328953;">Halo kak, pilihan bus nya apa saja ya ?</h3>
                                <p>Pilihan bus dapat diilhat di <a href="{{ url('/bus')}}">link ini</a></p>
                                <div class="single_destination mt-3">
                                    <h4 style="font-family: Arial; font-size: 18px; font-style: normal; font-variant: normal; font-weight: 700;color:#328953;">Kalau fasilitas bus nya apa saja ya kak?</h4>
                                    Fasilitas standar setiap bus adalah AC, Kotak P3K, Speaker. Untuk fasilitas tambahan dapat mengontak admin kami melalui whatsapp &#128578
                                </div>
                                <div class="single_destination mt-3">
                                    <h4 style="font-family: Arial; font-size: 18px; font-style: normal; font-variant: normal; font-weight: 700;color:#328953;">Untuk tujuannya bisa kemana saja ya kak?</h4>
                                    Untuk tujuan, kita melayani hampir seluruh kota-kota besar di Jawa dan Bali, bisa kontak amdin kita melalui whatsapp untuk info lebih lanjut yah &#128578
                                </div>
                                <div class="single_destination mt-3">
                                    <h4 style="font-family: Arial; font-size: 18px; font-style: normal; font-variant: normal; font-weight: 700;color:#328953;">Lagi COVID-19 gini, standar kesehatannya gimana kak?</h4>
                                    Merujuk pada peraturan pemerintah mengenai protokol kesehatan, kita mengikuti prosedur yang diberikan pemerintah. Setiap perjalanan yang akan dilakukan akan disemprot dengan <i>desinfectant</i> terlebih dahulu untuk memastikan sanitasi setiap armada.
                                </div>
                                <div class="single_destination mt-3">
                                    <h4 style="font-family: Arial; font-size: 18px; font-style: normal; font-variant: normal; font-weight: 700;color:#328953;">Apa untungnya berlangganan newsletter suryaputra?</h4>
                                    Dengan berlanggan <i>newsletter</i> suryaputra, kamu akan dapetin informasi terbaru loh &#128521
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <!-- ================ contact section end ================= -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <a href="{{$pageConfiguration->whatsapp_api}}&text=Halo%20Suryaputra%21%20Mau%20nanya%20informasi%20lebih%20lanjut%20nih." class="float" target="_blank">
        <i class="fa fa-whatsapp my-float"></i>
    </a>
    <!-- footer start -->
    @include('includes.footer')
    <!--/ footer end  -->
    <!-- Modal -->
    <div class="modal fade custom_search_pop" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="serch_form">
                    <input type="text" placeholder="Search">
                    <button type="submit">search</button>
                </div>
            </div>
        </div>
    </div>
    <!-- JS here -->
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/ajax-form.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/scrollIt.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/nice-select.min.js"></script>
    <script src="js/jquery.slicknav.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/gijgo.min.js"></script>
    <script src="{{ asset('racks/js/navigation.js')}}"></script>
    <!--contact js-->
    <script src="js/contact.js"></script>
    <script src="js/jquery.ajaxchimp.min.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/mail-script.js"></script>
    <script src="js/main.js"></script>
    <script>
        $('#datepicker').datepicker({
            iconsLibrary: 'fontawesome',
            icons: {
                rightIcon: '<span class="fa fa-caret-down"></span>'
            }
        });
        $('#datepicker2').datepicker({
            iconsLibrary: 'fontawesome',
            icons: {
                rightIcon: '<span class="fa fa-caret-down"></span>'
            }

        });
    </script>
</body>

</html>