<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin | Detail Bus</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ekko Lightbox -->
  <link rel="stylesheet" href="{{ asset('plugins/ekko-lightbox/ekko-lightbox.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="{{ asset('plugins/lightbox/css/lightbox.css')}}">
</head>

<body class="hold-transition sidebar-mini">
  <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
          <a href="index3.html" class="nav-link">Home</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
          <a href="#" class="nav-link">Contact</a>
        </li>
      </ul>

      <!-- SEARCH FORM -->
      <form class="form-inline ml-3">
        <div class="input-group input-group-sm">
          <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-navbar" type="submit">
              <i class="fas fa-search"></i>
            </button>
          </div>
        </div>
      </form>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    @include('admin.includes.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Detail</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Detail</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12 col-sm-4">
              <div class="info-box bg-light">
                <div class="info-box-content">
                  <span class="info-box-text text-center text-muted">Nama Bus</span>
                  <span class="info-box-number text-center text-muted mb-0">{{$bus->name}}</span>
                </div>
              </div>
            </div>
            <div class="col-12 col-sm-4">
              <div class="info-box bg-light">
                <div class="info-box-content">
                  <span class="info-box-text text-center text-muted">Kapasitas Tempat Duduk</span>
                  <span class="info-box-number text-center text-muted mb-0">{{$bus->number_of_seats}}</span>
                </div>
              </div>
            </div>
            <div class="col-12">
              <div class="card card-primary">
                <div class="card-header">
                  <div class="card-title">
                    {{'Detail' .' '. $bus->name}}
                  </div>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-12">
                      @include('admin.includes.flash-message')
                    </div>
                  </div>

                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-busDetail-create" style="margin-bottom:10px;">Tambah Detail Gambar</button>

                  <div class="row">
                    <div class="col-sm-2">
                      <a href="{{'https://drive.google.com/uc?export=view&id='.$bus->image_url}}" data-toggle="lightbox" data-title="Foto Utama" data-gallery="gallery">
                        <img src="{{'https://drive.google.com/uc?export=view&id='.$bus->image_url}}" class="img-fluid mb-2" alt="white sample" />
                      </a>
                    </div>
                    @foreach($busDetail as $busDetails)
                    <div class="col-sm-2">
                      <a href="{{'https://drive.google.com/uc?export=view&id='.$busDetails->image_url}}" data-lightbox="image-{{$bus->id}}">
                        <img src="{{'https://drive.google.com/uc?export=view&id='.$busDetails->image_url}}" class="img-fluid mb-2" alt="Masalah saat memuat gambar">
                      </a>
                    </div>
                    @endforeach
                    <table id="example2" class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <th>Nama</th>
                          <th>ID File Google Drive</th>
                          <th>Gambar</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($busDetail as $busDetails)
                        <tr>
                          <td>{{$busDetails->name}}</td>
                          <td>{{$busDetails->image_url}}</td>
                          <td width="30%;">
                            <a href="{{'https://drive.google.com/uc?export=view&id='.$busDetails->image_url}}" data-toggle="lightbox" data-title="Gambar Utama Bus {{$busDetails->name}}" data-gallery="gallery">
                              <img src="{{'https://drive.google.com/uc?export=view&id='.$busDetails->image_url}}" class="img-fluid mb-2" alt="white sample" />
                            </a>
                          </td>
                          <td>
                            <button type="button" class="btn btn-primary mb-2" data-toggle="modal" data-target="#modal-busDetail-edit-{{$busDetails->id}}"><i class="fa fa-edit" aria-hidden="true"></i> Ubah</button>
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-busDetail-delete-{{$busDetails->id}}"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                      <tfoot>
                        <tr>
                          <th>Nama</th>
                          <th>ID File Google Drive</th>
                          <th>Gambar</th>
                          <th>Action</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <button type="button" class="btn btn-primary mb-2" onclick="window.location='{{ route("bus.index") }}'"><i class="fa fa-arrow-left" aria-hidden="true"></i>Kembali</button>
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
      <div class="float-right d-none d-sm-block">
        <b>Version</b> 3.0.4
      </div>
      <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
      reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <div class="modal fade" id="modal-busDetail-create">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Tambah Bus</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="{{ route('admin.create-detailbus')}}" method="post">
            @csrf
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Bus ID</label>
                  <input type="text" class="form-control" autocomplete="off" name="input_detailbus_bus_id" required readonly value="{{$bus->id}}">
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label>Nama Detail Gambar</label>
                  <input type="text" class="form-control" autocomplete="off" name="input_detailbus_name" required>
                </div>
              </div>


              <div class="col-md-12">
                <div class="form-group">
                  <label>ID File Google Drive</label>
                  <textarea rows="3" class="form-control" autocomplete="off" name="input_detailbus_image_url" required></textarea>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  @foreach($busDetail as $busDetail)
  <div class="modal fade" id="modal-busDetail-edit-{{$busDetail->id}}">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Edit Detail Bus</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="{{ route('admin.update-detailbus')}}" method="post">
            @csrf
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Kode Detail Bus</label>
                  <input type="text" class="form-control" autocomplete="off" readonly name="input_detailbus_id" value="{{$busDetail->id}}" required>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label>Nama Bus</label>
                  <input type="text" class="form-control" autocomplete="off" name="input_detailbus_name" value="{{$busDetail->name}}" required>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label>ID File Google Drive</label>
                  <textarea rows="3" class="form-control" autocomplete="off" name="input_detailbus_image_url" required>{{$busDetail->image_url}}</textarea>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <div class="modal fade" id="modal-busDetail-delete-{{$busDetail->id}}">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Hapus Detail Bus</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="{{ route('admin.delete-detailbus')}}" method="post">
            @csrf
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <input type="hidden" class="form-control" autocomplete="off" readonly name="input_detailbus_id" value="{{$busDetail->id}}" required>
                </div>
              </div>
              <div class="modal-body">
                <p>Anda mengkonfirmasi ingin menghapus detail {{$busDetail->name}} ?</p>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-warning">Hapus</button>
        </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  @endforeach

  <!-- jQuery -->
  <script src="{{ asset('plugins/jquery/jquery.min.js')}}"></script>
  <!-- Bootstrap -->
  <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <!-- jQuery UI -->
  <script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js')}}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('dist/js/adminlte.min.js')}}"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="{{ asset('dist/js/demo.js')}}"></script>
  <!-- Filterizr-->
  <script src="{{ asset('plugins/filterizr/jquery.filterizr.min.js')}}"></script>
  <!-- Page specific script -->
  <script src="{{ asset('plugins/lightbox/js/lightbox.js')}}"></script>
  <script>
    $(function() {
      $('.filter-container').filterizr({
        gutterPixels: 3
      });
      $('.btn[data-filter]').on('click', function() {
        $('.btn[data-filter]').removeClass('active');
        $(this).addClass('active');
      });
    })
  </script>
</body>

</html>