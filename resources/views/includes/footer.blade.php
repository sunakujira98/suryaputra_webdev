<div><img src="{{ asset('img/pelangi.png')}}" style="width:100%; position:relative; bottom:-10px;"></div>
<footer style="background-color:#3b528f;">
   <div class="container" style="color:white">
      <div class="row">
         <div class="col-md-3" style="font-size:12px; font-family: Kanit script=latin rev=1">
            <img class="footer-logo image-responsive" src="{{ asset('img/logo3.png')}}">
            <div class="footer-address">
               Jalan Soekarno Hatta No. 269</br>
               Kebon Lega, Bojongloa Kidul,</br>
               Bandung City, West Java 40235</br>
               0812-1480-8888</br>
               bussuryaputra@gmail.com
            </div>
         </div>
         <div class="col-md-3 footer-column">
            <h6 style="color:white">TENTANG KAMI</h6>
            <ul class="list-unstyled">
               <li><a href="{{ url('/aboutus')}}">Tentang Kami</a></li>
            </ul>
         </div>
         <div class="col-md-3 footer-column">
            <h6 style="color:white">PRODUK & LAYANAN</h6>
            <ul class="list-unstyled">
               <li><a href="{{ url('/bus')}}">Armada</a></li>
            </ul>
         </div>
         <div class="col-md-3 footer-column">
            <h6 style="color:white">KONTAK KAMI</h6>
            <ul class="list-unstyled">
               <li><a href="{{ url('contact')}}">FAQ</a></li>
            </ul>
         </div>
         <div class="col-md-2 footer-column">
            <!-- <div class=" d-flex">
               <a class="social-media" href="{{$pageConfiguration->instagram_url1}}" target="_blank">
                  <i class="fa fa-instagram fa-lg"></i>
               </a>
               <a class="social-media" href="{{$pageConfiguration->instagram_url1}}" target="_blank">
                  <i class="fa fa-instagram fa-lg"></i>
               </a>
               <a class="social-media" href="{{$pageConfiguration->twitter_url}}" target="_blank">
                  <i class="fa fa-twitter fa-lg"></i>
               </a>
               <a class="social-media" href="{{$pageConfiguration->facebook_url}}" target="_blank">
                  <i class="fa fa-facebook fa-lg"></i>
               </a>
               <a class="social-media" href="{{$pageConfiguration->youtube_url}}" target="_blank">
                  <i class="fa fa-youtube-play fa-lg"></i>
               </a>
            </div> -->
         </div>
      </div>
      <div class="row">
         <div class="col-md-12 text-center copyright">
            <p>
               <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
               <!-- Copyright &copy;<script>
                  document.write(new Date().getFullYear());
               </script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://suryaputra.co" target="_blank">Suryaputra</a> -->
               <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </p>
         </div>
      </div>
   </div>
</footer>