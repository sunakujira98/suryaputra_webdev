<header class="header-section">
<nav class="navbar navbar-expand-sm bg-green navbar-dark fixed-top">
  <a href="{{ url('/')}}" class="banner-logo" style="margin-top: -20px">
         <img src="{{ asset('img/logo_horizontal.png')}}" class="responsive-logo d-none d-xl-block" />
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
   </ul>
     <ul class="navbar-nav">
         <li class="nav-item" id="right-40" >
             <a class="nav-link" href="{{ url('/') }}">BERANDA</a>
         </li>
         <li class="nav-item" id="right-40">
             <a class="nav-link" href="{{ url('/aboutus')}}">TENTANG KAMI</a>
         </li>
         <li class="nav-item" id="right-40">
             <a class="nav-link" href="{{ url('/bus')}}">ARMADA</a>
         </li>
         <li class="nav-item" id="right-40">
             <a class="nav-link" href="{{ url('/promotion')}}">PROMOSI</a>
         </li>
         <li class="nav-item" id="right-40">
             <a class="nav-link" href="{{ url('blog')}}">ARTIKEL</a>
         </li>
         <li class="nav-item" id="right-40">
             <a class="nav-link" href="{{ url('contact')}}">KONTAK KAMI</a>
         </li>
     </ul>
  </div>
</nav>
</header>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-41839748-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-41839748-1');
</script>
