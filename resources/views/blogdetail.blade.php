<!doctype html>
<html class="no-js" lang="zxx">

<head>
   <meta charset="utf-8">
   <meta http-equiv="x-ua-compatible" content="ie=edge">
   <title>Suryaputra Bus Pariwisata - Artikel Detail</title>
   @foreach($metaArticleDetail as $metaPages)
   <meta name="{{$metaPages->meta_name}}" content="{{$metaPages->meta_content}}">
   @endforeach
   <meta name="viewport" content="width=device-width, initial-scale=1">
   @include('includes.meta')

   <!-- <link rel="manifest" href="site.webmanifest"> -->
   <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
   <!-- Place favicon.ico in the root directory -->

   <!-- CSS here -->
   <link rel="stylesheet" href="{{ asset('fonts/kanitsemibold/stylesheet.css')}}">
   <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" href="css/owl.carousel.min.css">
   <link rel="stylesheet" href="css/magnific-popup.css">
   <link rel="stylesheet" href="css/font-awesome.min.css">
   <link rel="stylesheet" href="css/themify-icons.css">
   <link rel="stylesheet" href="css/nice-select.css">
   <link rel="stylesheet" href="css/flaticon.css">
   <link rel="stylesheet" href="css/gijgo.css">
   <link rel="stylesheet" href="css/animate.css">
   <link rel="stylesheet" href="css/slicknav.css">
   <link rel="stylesheet" href="css/style.css">
   <link rel="stylesheet" href="{{ asset('fonts/ppwoodland/stylesheet.css')}}">
   <link rel="stylesheet" href="{{ asset('racks/css/style.css')}}">
   <link rel="stylesheet" href="{{ asset('racks/css/custom.css')}}">
   <script src="{{ asset('racks/js/navigation.js')}}"></script>
   <!-- <link rel="stylesheet" href="css/responsive.css"> -->
   <style>
      .float {
         position: fixed;
         width: 60px;
         height: 60px;
         bottom: 40px;
         right: 40px;
         background-color: #25d366;
         color: #FFF;
         border-radius: 50px;
         text-align: center;
         font-size: 30px;
         box-shadow: 2px 2px 3px #999;
         z-index: 100;
      }

      .my-float {
         margin-top: 16px;
      }
   </style>
</head>

<body>
   @include('includes.header')
   <!-- Banner-->
   <div class="site-blocks-cover overlay background-dynamic" data-aos="fade" id="home-section">
      <div class="banner-title">
         <h1 class="text color-responsive" data-aos="fade-up">Artikel Detail</h1>
         <hr class="text-center hr-color">
      </div>
   </div>
   <img src=" {{url('img/block.PNG')}}" width="100%;" height="50px;">

   <!--================Blog Area =================-->
   <section class="blog_area single-post-area section-padding">
      <div class="container">
         @include('includes.flash-message')
         <div class="row">
            <div class="col-lg-8 posts-list">
               <div class="single-post">
                  <div class="feature-img">
                     <img class="img-fluid" src="{{$articleDetail->image_url}}" alt="">
                  </div>
                  <div class="blog_details">
                     <h2 style="font-family: Arial; font-size: 22px; font-style: normal; font-variant: normal; font-weight: 700;color:#328953;">{{$articleDetail->title}}
                     </h2>
                     <ul class="blog-info-link mt-3 mb-4">
                        <li><a href="#"><i class="fa fa-user"></i> {{$articleDetail->category->name}}</a></li>
                     </ul>
                     <span style="font-family: Arial !important; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 600; line-height: 20px;">
                        {!! $articleDetail->content !!}
                     </span>
                     <div class="quote-wrapper">
                        <div class="quotes">
                           {!! $articleDetail->quote !!}
                        </div>
                     </div>
                  </div>
               </div>
               <div class="navigation-top">
                  <div class="d-sm-flex justify-content-between text-center">
                     <!-- <p class="like-info"><span class="align-middle"><i class="fa fa-heart"></i></span> Lily and 4
                        people like this</p> -->
                     <div class="col-sm-4 text-center my-2 my-sm-0">
                        <!-- <p class="comment-count"><span class="align-middle"><i class="fa fa-comment"></i></span> 06 Comments</p> -->
                     </div>
                     <ul class="social-icons">
                        <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        <li><a href="#"><i class="fa fa-behance"></i></a></li>
                     </ul>
                  </div>
                  <div class="navigation-area">
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-12 nav-left flex-row d-flex justify-content-start align-items-center">
                           <div class="thumb">
                              <a href="#">
                                 <img class="img-fluid" src="img/post/preview.png" alt="">
                              </a>
                           </div>
                           <div class="arrow">
                              <a href="#">
                                 <span class="lnr text-white ti-arrow-left"></span>
                              </a>
                           </div>
                           <div class="detials">
                              <p style="font-family: Arial; font-size: 18px; font-style: normal; font-variant: normal; font-weight: 600; line-height: 20px;">Artikel Sebelumnya</p>
                              @if($prevPage!=NULL)
                              <a href="{{ url('/blogdetail-'.$nextPage->id.'-'.$nextPage->slug)}}">
                                 <h4 style="font-family: Arial; font-size: 15px; font-style: normal; font-variant: normal; font-weight: 600; line-height: 20px;">Space The Final Frontier</h4>
                              </a>
                              @endif
                           </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 nav-right flex-row d-flex justify-content-end align-items-center">
                           <div class="detials">
                              <p style="font-family: Arial; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 600; line-height: 20px;">Artikel Berikutnya</p>
                              @if($nextPage!=NULL)
                              <a href="{{ url('/blogdetail-'.$nextPage->id.'-'.$nextPage->slug)}}">
                                 <h4 style="font-family: Arial; font-size: 15px; font-style: normal; font-variant: normal; font-weight: 600; line-height: 20px;">{{$nextPage->name}}</h4>
                              </a>
                              @endif
                           </div>
                           <div class="arrow">
                              <a href="#">
                                 <span class="lnr text-white ti-arrow-right"></span>
                              </a>
                           </div>
                           <div class="thumb">
                              <a href="#">
                                 <img class="img-fluid" src="img/post/next.png" alt="">
                              </a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="blog-author">
                  <div class="media align-items-center">
                     <div class="media-body">
                        <a href="#">
                           <h4 style="font-family: Arial; font-size: 18px; font-style: normal; font-variant: normal; font-weight: 600; line-height: 20px;">Suryaputra Admin</h4>
                        </a>
                        <p style="font-family: Arial; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 600; line-height: 20px;">Suryaputra adalah jasa penyewaan bus pariwisata..</p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-lg-4">
               <div class="blog_right_sidebar">
                  <aside class="single_sidebar_widget post_category_widget">
                     <h4 class="widget_title" style="font-family: Arial; font-size: 22px; font-style: normal; font-variant: normal; font-weight: 700;color:#328953;">Category</h4>
                     <ul class="list cat-list">
                        @foreach($category as $categories)
                        <li>
                           <a href="#" class="d-flex">
                              <p style="font-family: Arial; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 600; line-height: 20px;">{{$categories->name}}</p>
                           </a>
                        </li>
                        @endforeach
                     </ul>
                  </aside>
                  <aside class=" single_sidebar_widget popular_post_widget">
                     <h3 class="widget_title" class="widget_title" style="font-family: Arial; font-size: 22px; font-style: normal; font-variant: normal; font-weight: 700;color:#328953;">Artikel Terbaru</h3>
                     @foreach($recentArticle as $recents)
                     <div class="media post_item">
                        <img src="{{'https://drive.google.com/uc?export=view&id='.$recents->image_url}}" alt="post" style="max-width:80px;">
                        <div class="media-body">
                           <a href="{{ url('/blogdetail-'.$recents->id.'-'.$recents->slug)}}">
                              <h3 style="font-family: Arial; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 600; line-height: 20px;">{{$recents->title}}</h3>
                           </a>
                           <p>{{$recents->created_at->format('d M Y')}}</p>
                        </div>
                     </div>
                     @endforeach
                  </aside>
                  <aside class="single_sidebar_widget tag_cloud_widget">
                     <h4 class="widget_title" style="font-family: Arial; font-size: 22px; font-style: normal; font-variant: normal; font-weight: 700;color:#328953;">Tag</h4>
                     <ul class="list">
                        @foreach($articleDetail->tags as $tags)
                        <li>
                           <a href="#">{{$tags->name}}</a>
                        </li>
                        @endforeach
                     </ul>
                  </aside>
                  <aside class="single_sidebar_widget newsletter_widget">
                     <h4 class="widget_title" style="font-family: Arial; font-size: 22px; font-style: normal; font-variant: normal; font-weight: 700;color:#328953;">Newsletter</h4>
                     <form action="{{ route('subscribe')}}" method="post">
                        @csrf
                        <div class="form-group">
                           <input type="email" class="form-control" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email'" placeholder='Enter email' name="email" required>
                        </div>
                        <button class="button rounded-0 primary-bg text-white w-100 btn_1 boxed-btn" type="submit">Subscribe</button>
                     </form>
                  </aside>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!--================ Blog Area end =================-->

   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
   <a href="{{$pageConfiguration->whatsapp_api}}&text=Halo%20Suryaputra%21%20Mau%20nanya%20informasi%20lebih%20lanjut%20nih." class="float" target="_blank">
      <i class="fa fa-whatsapp my-float"></i>
   </a>

   <!-- footer start -->
   @include('includes.footer')
   <!--/ footer end  -->

   <!-- Modal -->
   <div class="modal fade custom_search_pop" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
            <div class="serch_form">
               <input type="text" placeholder="Search">
               <button type="submit">search</button>
            </div>
         </div>
      </div>
   </div>

   <!-- JS here -->
   <script src="js/vendor/modernizr-3.5.0.min.js"></script>
   <script src="js/vendor/jquery-1.12.4.min.js"></script>
   <script src="js/popper.min.js"></script>
   <script src="js/bootstrap.min.js"></script>
   <script src="js/owl.carousel.min.js"></script>
   <script src="js/isotope.pkgd.min.js"></script>
   <script src="js/ajax-form.js"></script>
   <script src="js/waypoints.min.js"></script>
   <script src="js/jquery.counterup.min.js"></script>
   <script src="js/imagesloaded.pkgd.min.js"></script>
   <script src="js/scrollIt.js"></script>
   <script src="js/jquery.scrollUp.min.js"></script>
   <script src="js/wow.min.js"></script>
   <script src="js/nice-select.min.js"></script>
   <script src="js/jquery.slicknav.min.js"></script>
   <script src="js/jquery.magnific-popup.min.js"></script>
   <script src="js/plugins.js"></script>
   <script src="js/gijgo.min.js"></script>

   <!--contact js-->
   <script src="js/contact.js"></script>
   <script src="js/jquery.ajaxchimp.min.js"></script>
   <script src="js/jquery.form.js"></script>
   <script src="js/jquery.validate.min.js"></script>
   <script src="js/mail-script.js"></script>

   <script src="js/main.js"></script>

</body>

</html>