<!doctype html>
<html class="no-js" lang="zxx">

<head>
   <meta charset="utf-8">
   <meta http-equiv="x-ua-compatible" content="ie=edge">
   <title>Suryaputra Bus Pariwisata - Tentang Kami</title>
   <meta name="description" content="">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   @include('includes.meta')
   @include('includes.meta-pages')
   <!-- <link rel="manifest" href="site.webmanifest"> -->
   <link rel="shortcut icon" type="image/x-icon" href="img/logo4.png">
   <!-- Place favicon.ico in the root directory -->
   <!-- CSS here -->
   <link rel="stylesheet" href="{{ asset('fonts/ppwoodland/stylesheet.css')}}">
   <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Arial" />
   <link rel="stylesheet" href="{{ asset('fonts/kanitsemibold/stylesheet.css')}}">
   <link rel="stylesheet" href="{{ asset('racks/css/animate.css')}}">
   <link rel="stylesheet" href="{{ asset('racks/css/owl.carousel.min.css')}}">
   <link rel="stylesheet" href="{{ asset('racks/css/owl.theme.default.min.css')}}">
   <link rel="stylesheet" href="{{ asset('racks/css/jquery.timepicker.css')}}">
   <link rel="stylesheet" href="css/font-awesome.min.css">
   <link rel="stylesheet" href="{{ asset('racks/css/style.css')}}">
   <link rel="stylesheet" href="{{ asset('racks/css/custom.css')}}">
   <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css">
   <link rel="stylesheet" href="css/aboutus.css">
</head>

<body>
   @include('includes.header')
   <!-- Banner-->
   <div class="site-blocks-cover overlay background-dynamic" data-aos="fade" id="home-section">
      <!-- <div class="banner-page">
         <h1 class="color-responsive" style="margin-top:-135%;">Tentang Kami</h1>
         <hr class="text-center" style="border-bottom: 4px solid white; width:120px; margin:auto">
      </div> -->
      <div class="banner-title">
         <h1 class="text color-responsive" data-aos="fade-up">Tentang Kami</h1>
         <hr class="text-center hr-color">
      </div>
   </div>
   <img src=" {{url('img/block.PNG')}}" width="100%;" height="50px;">

   <section class="ftco-section ftco-degree-bg">
      <div class="container">
         <div class="row justify-content-center section-title">
            <div class="col-md-7 text-center heading-section ftco-animate">
               <h2 style="font-family: Kanit script=latin rev=1; font-size: 22px; font-style: normal; font-variant: normal; font-weight: 600;color:#090109;">PT. SURYAPUTRA ANUGERAH</h2>
               <hr class="text-center" style="border-bottom: 4px solid #328953; width:80px; margin:auto">
            </div>
         </div>
         <div class="row justify-content-center">
            <div class="col-md-6 text-center heading-section ftco-animate">
               <p style="font-family: Kanit script=latin rev=1; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 20px; color:#090109; text-align: justify;">Suryaputra adalah jasa penyewaan transportasi sesuai kebutuhan pelanggan di wilayah Pulau Jawa, Sumatera, Bali dan Lombok dengan armada berjumlah lebih dari 100 armada yang terdiri dari bus besar, bus ukuran sedang, bus mikro, kendaraan niaga, dan kendaraan dengan pesanan kebutuhan khusus.</p>
            </div>
         </div>
         <div class="row justify-content-center" style="margin-top:80px;">
            <div class="col-md-6 text-center heading-section ftco-animate content-text">
               <h2 style="font-family: Kanit script=latin rev=1; font-size: 22px; font-style: normal; font-variant: normal; font-weight: 600;color:#090109; text-align:center;">VISION & MISION</h2>
               <p style="font-family: Kanit script=latin rev=1; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 20px; color:#090109 ;text-align:justify;">Kami berupaya untuk selalu meningkatkan kinerja kami, sehingga dapat memberikan pelayanan yang terbaik bagi pelanggan, yang dapat disesuaikan dengan kebutuhan masing-masing, dengan mengedepankan keselamatan dan kenyamanan dalam berkendaraan, demi menjaga loyalitas pelanggan dan menciptakan nilai tambah bagi Suryaputra dan seluruh pelanggannya.</p>
               <!-- <div>
                  <img src="img/achievement/achieve1.jpeg" alt="" style="width:100%">
               </div> -->
            </div>
            <div class="col-md-6 heading-section ftco-animate content-text">
               <h2 style="font-family: Kanit script=latin rev=1; font-size: 22px; font-style: normal; font-variant: normal; font-weight: 600;color:#090109; text-align:center">ACHIEVEMENTS</h2>
               <p style="font-family: Kanit script=latin rev=1; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 20px; color:#090109 ;text-align:justify">Kami mendapatkan pencapaian dari ISO 9001:2015 yang menyatakan bahwa PT. SURYAPUTRA ANUGERAH <i>"telah menerapkan sistem manajemen mutu yang memenuhi ISO 9001:2015"</i>. Lalu mendapatkan pengharaan dari dishub pada tahun 2019 dalam kategori <i>"Perusahaan yang Mendukung Penyelenggaraan Mudik Gratis Tahun 2019"</i>.</p>
               <!-- <div>
                  <img src="img/achievement/achieve2.jpg" alt="" style="width:100%">
               </div>
               <div style="margin-top:20px">
                  <img src="img/achievement/achieve3.jpeg" alt="" style="width:100%">
               </div> -->
            </div>
         </div>
      </div>
   </section>

   <section class="ftco-section-experience ftco-no-pb-experience ftco-no-pt-experience bg-primary-experience" style="margin-top:10px;">
      <div class="container">
         <div class="row justify-content-center">
            <div class="col-md-7 text-center mt-5">
               <h2 style="font-family: Kanit script=latin rev=1; font-size: 22px; font-style: normal; font-variant: normal; font-weight: 600;color:#090109;">PENGALAMAN & PENGHARGAAN KAMI</h2>
               <hr class="text-center" style="border-bottom: 4px solid #328953; width:80px; margin:auto">
            </div>
         </div>
         <div class="row" style="padding-bottom:40px;">
            <div class="col-md-3" style="text-align:center">
               <a data-target="#pengalaman-1" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">
                  <img src="{{ asset('img/experience/400px-2018_Asian_Games_logo (1).png')}}" alt="" style="width:100px;height:130px">
               </a>
            </div>
            <div class="col-md-3" style="text-align:center">
               <a data-target="#pengalaman-2" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">
                  <img src="{{ asset('img/experience/wonderful indonesia (1).png')}}" alt="" style="width:100%; padding-top:20px">
               </a>
            </div>
            <div class="col-md-3" style="text-align:center">
               <a data-target="#pengalaman-3" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">
                  <img src="{{ asset('img/experience/pon jabar (1).png')}}" alt="" style="width:100%">
               </a>
            </div>
            <div class="col-md-3" style="text-align:center">
               <a data-target="#pengalaman-4" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">
                  <img src="{{ asset('img/experience/Logo-KAA (1).png')}}" alt="" style="width:120px;height:150px">
               </a>
            </div>
         </div>
         <div class="row" style="padding-bottom:40px;">
            <div class="col-md-3" style="text-align:center">
               <a data-target="#penghargaan-1" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">
                  <img src="{{ asset('img/achievement/achieve2.jpg')}}" alt="" style="width:170px; height:110px; margin-top:55px">
               </a>
            </div>
            <div class="col-md-3" style="text-align:center">
               <a data-target="#penghargaan-2" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">
                  <img src="{{ asset('img/achievement/achieve3.jpeg')}}" alt="" style="width:170px; height:110px; margin-top:55px">
               </a>
            </div>
            <div class="col-md-3" style="text-align:center">
               <a data-target="#penghargaan-3" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">
                  <img src="{{ asset('img/achievement/achieve4.png')}}" alt="" style="width:100%;">
               </a>
            </div>
            <div class="col-md-3" style="text-align:center">
               <a data-target="#penghargaan-4" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">
                  <img src="{{ asset('img/achievement/achieve1.jpeg')}}" alt="" style="width:100px; height:165px">
               </a>
            </div>
         </div>
      </div>
   </section>

   <section class="ftco-section ftco-degree-bg">
      <div class="container">
         <div class="row justify-content-center mb-2 pb-5">
            <div class="col-md-7 text-center heading-section ftco-animate">
               <h2 style="font-family: Kanit script=latin rev=1; font-size: 22px; font-style: normal; font-variant: normal; font-weight: 600;color:#090109;">ALASAN UNTUK MEMILIH KAMI</h2>
               <hr class="text-center" style="border-bottom: 4px solid #328953; width:80px; margin:auto">
            </div>
         </div>
         <div class="row justify-content-center">
            <div class="col-md-4">
               <div class="media block-6 services d-block text-center">
                  <div class="d-flex justify-content-center">
                     <img src="img/svg_icon/bus-01_64.png" alt="">
                  </div>
                  <div class="media-body p-2">
                     <h3 class="heading" style="font-family: Kanit script=latin rev=1;font-weight: 600;color:#090109;">Comfortable Journey</h3>
                     <p style="font-family: Kanit script=latin rev=1; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 20px; color:#090109">Menyediakan perjalanan yang aman untuk setiap penumpang kami, karena penumpang begitu berharga bagi kami.</p>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="media block-6 services d-block text-center">
                  <div class="d-flex justify-content-center">
                     <img src="img/svg_icon/care-01_64.png" alt="" style="height:66px;">
                  </div>
                  <div class="media-body p-2">
                     <h3 class="heading" style="font-family: Kanit script=latin rev=1;font-weight: 600;color:#090109;">Care</h3>
                     <p style="font-family: Kanit script=latin rev=1; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 20px; color:#090109">Kami siap melayani Anda sepenuh hati. Mulai dari pemesanan armada hingga mencapai destinasi tujuan, melayani Anda dengan kualtias terbaik adalah prioritas kami.</p>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="media block-6 services d-block text-center">
                  <div class="d-flex justify-content-center">
                     <img src="img/svg_icon/shield-01-01.png" alt="" style="height:66px;">
                  </div>
                  <div class="media-body p-2">
                     <h3 class="heading" style="font-family: Kanit script=latin rev=1;font-weight: 600;color:#090109;">Health & Safety</h3>
                     <p style="font-family: Kanit script=latin rev=1; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 20px; color:#090109">Kami memahami kekhawatiran Anda sehingga kami ingin Anda mengetahui bahwa setiap armada kamitelah dengan rutin di disinfeksi mengikuti protokol kesehatan nasional yang ada.</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="ftco-section ftco-degree-bg">
      <div class="container">
         <div class="row justify-content-center" style="margin-top:80px;">
            <div class="col-md-6 text-center heading-section ftco-animate content-text">
               <h2 style="font-family: Kanit script=latin rev=1; font-size: 22px; font-style: normal; font-variant: normal; font-weight: 600;color:#090109; text-align:center">OUR SPECIALITY</h2>
               <p style="font-family: Kanit script=latin rev=1; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 20px; color:#090109;text-align:justify;">Setiap armada kami dilengkapi berbagai fasilitas terbaik untuk memberikan kenyamanan dan keamanan berkendara dari pelanggan yang mencakup GPS (<i>Global Positioning System</i>, AC, LED/LCD TV, Hard-drive, blu-ray dan karaoke dengan <i>sound system</i> terbaik , reclining seat, kursi pijat, bantal, selimut, leg rest, foot reset, kotak pendingin minuman ringan, coffee maker, charger headphone, serta berbagai perlengkapan pengamanan keselamatan seperti perangkat P3K, alat pemadam kebakaran ringan (APAR), palu pemecah kaca dan pintu darurat samping dan atas kendaraan. Selain itu setiap kursi penumpang juga dilengkapi dengan sabuk pengaman. Fasilitas diatas dapat disesuaikan dengan permintaan pelanggan kami, kecuali perangkat keselamatan menjadi kewajiban di setiap armada..</p>
            </div>
            <div class="col-md-6 heading-section ftco-animate content-text">
               <img src="img/speciality.JPG" alt="bus pariwisata terbaik bandung" style="width:100%">
            </div>
         </div>
      </div>
   </section>
   <section class="ftco-section ftco-degree-bg">
      <div class="container">
         <div class="row justify-content-center" style="margin-top:80px;">
            <div class="col-md-6 heading-section ftco-animate content-text">
               <img src="img/team.JPG" alt="bus pariwisata terbaik bandung" style="width:100%">
            </div>
            <div class="col-md-6 text-center heading-section ftco-animate content-text">
               <h2 style="font-family: Kanit script=latin rev=1; font-size: 22px; font-style: normal; font-variant: normal; font-weight: 600;color:#090109; text-align:center">OUR TEAM</h2>
               <p style="font-family: Kanit script=latin rev=1; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 20px; color:#090109;text-align:justify;">Kami berupaya untuk selalu meningkatkan kinerja kami, sehingga dapat memberikan pelayanan yang terbaik bagi pelanggan, yang dapat disesuaikan degnan kebutuhan masing-masing, dengan mengedepankan keselamatan dan kenyamanan dalam berkendaraan, demi menjaga loyalitas pelanggan dan menciptakan nilai tambah bagi Suryaputra dan seluruh pelanggannya.</p>
            </div>
         </div>
      </div>
   </section>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
   <a href="{{$pageConfiguration->whatsapp_api}}&text=Halo%20Suryaputra%21%20Mau%20nanya%20informasi%20lebih%20lanjut%20nih." class="float" target="_blank">
      <i class="fa fa-whatsapp my-float"></i>
   </a>
   @include('includes.footer')

   @for($i=1;$i<=4;$i++) @php if($i==1) { $image_url=asset('img/experience/400px-2018_Asian_Games_logo (1).png'); } else if($i==2) { $image_url=asset('img/experience/wonderful indonesia (1).png'); } else if($i==3) { $image_url=asset('img/experience/pon jabar (1).png'); } else { $image_url=asset('img/experience/Logo-KAA (1).png'); } @endphp <div class="modal fade" id="pengalaman-{{$i}}">
      <div class="modal-dialog modal-xl">
         <div class="modal-content">
            <div class="modal-header">
               <h4 class="modal-title">Keterangan Pengalaman</h4>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div class="modal-body">
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <img src="{{$image_url}}" alt="Detail Keterangan Penghargaan" style="display:block;margin-left:auto;margin-right:auto;width:80%;">
                     </div>
                  </div>

                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Keterangan</label>
                        Selama lebih dari beberapa tahun . . .
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal-footer justify-content-between">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </div>
         <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
      @endfor

      @for($i=1;$i<=4;$i++) @php if($i==1) { $image_url=asset('img/achievement/achieve2.jpg'); } else if($i==2) { $image_url=asset('img/achievement/achieve3.jpeg'); } else if($i==3) { $image_url=asset('img/achievement/achieve4.png'); } else { $image_url=asset('img/achievement/achieve1.jpeg'); } @endphp <div class="modal fade" id="penghargaan-{{$i}}">
         <div class="modal-dialog modal-xl">
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title">Keterangan Penghargaan</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <img src="{{$image_url}}" alt="Detail Keterangan Penghargaan" style="display:block;margin-left:auto;margin-right:auto;width:80%;">
                        </div>
                     </div>

                     <div class="col-md-12">
                        <div class="form-group">
                           <label>Keterangan</label>
                           Selama lebih dari beberapa tahun . . .
                        </div>
                     </div>
                  </div>
               </div>
               <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
            <!-- /.modal-content -->
         </div>
         <!-- /.modal-dialog -->
         </div>
         <!-- /.modal -->
         @endfor

         <!-- JS here -->
         <!-- loader -->
         <script src="{{ asset('racks/js/jquery.min.js')}}"></script>
         <script src="{{ asset('racks/js/jquery-migrate-3.0.1.min.js')}}"></script>
         <script src="{{ asset('racks/js/popper.min.js')}}"></script>
         <script src="{{ asset('racks/js/bootstrap.min.js')}}"></script>
         <script src="{{ asset('racks/js/jquery.easing.1.3.js')}}"></script>
         <script src="{{ asset('racks/js/jquery.waypoints.min.js')}}"></script>
         <script src="{{ asset('racks/js/jquery.stellar.min.js')}}"></script>
         <script src="{{ asset('racks/js/owl.carousel.min.js')}}"></script>
         <script src="{{ asset('racks/js/jquery.magnific-popup.min.js')}}"></script>
         <script src="{{ asset('racks/js/aos.js')}}"></script>
         <script src="{{ asset('racks/js/navigation.js')}}"></script>
         <script src="racks/js/jquery.animateNumber.min.js"></script>
         <script src="racks/js/bootstrap-datepicker.js"></script>
         <script src="racks/js/jquery.timepicker.min.js"></script>
         <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
         <script src="racks/js/google-map.js"></script>
         <script src="racks/js/main.js"></script>
         <!--====== Javascripts & Jquery ======-->
         <script src="endgam/js/jquery.slicknav.min.js"></script>
         <script src="endgam/js/main.js"></script>
         <!--contact js-->
         <script src="js/contact.js"></script>
         <script src="js/jquery.ajaxchimp.min.js"></script>
         <script src="js/jquery.form.js"></script>
         <script src="js/jquery.validate.min.js"></script>
         <script src="js/mail-script.js"></script>
         <script src="js/main.js"></script>
         <script>
            $('#datepicker').datepicker({
               iconsLibrary: 'fontawesome',
               icons: {
                  rightIcon: '<span class="fa fa-caret-down"></span>'
               }
            });
         </script>
</body>

</html>