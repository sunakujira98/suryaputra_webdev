<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Suryaputra promotion Pariwisata - Promosi</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('includes.meta')
    @include('includes.meta-pages')
    <!-- <link rel="manifest" href="site.webmanifest"> -->
    <link rel="shortcut icon" type="image/x-icon" href="img/logo4.png">
    <!-- Place favicon.ico in the root directory -->
    <!-- CSS here -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('fonts/kanitsemibold/stylesheet.css')}}">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Arial" />
    <link rel="stylesheet" href="{{ asset('racks/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('racks/css/custom.css')}}">
    <link rel="stylesheet" href="css/style.css">
    <!-- <link rel="stylesheet" href="css/responsive.css"> -->
</head>

<body>
    @include('includes.header')
    <!-- Banner-->
    <div class="site-blocks-cover overlay background-dynamic" data-aos="fade" id="home-section">
        <div class="banner-title">
            <h1 class="text color-responsive" data-aos="fade-up">Promosi Kami</h1>
            <hr class="text-center hr-color">
        </div>
    </div>
    <img src=" {{url('img/block.PNG')}}" width="100%;" height="50px;">

    <div class="popular_destination_area">
        <div class="container">
            <div class="row">
                @foreach($promotion as $promotions)
                <div class="col-lg-4 col-md-6">
                    @php
                    $hypehenSentece = \Util::hypehenize($promotions->name);
                    @endphp
                    <a href="{{ url('/promotion-'.$promotions->id)}}">
                        <div class="single_destination">
                            <div class="thumb">
                                <img src="{{'https://drive.google.com/uc?export=view&id='.$promotions->image_url}}" alt="{{$promotions->name}}" style="max-height: 300px;">
                            </div>

                            <div class="content">
                                <p class="d-flex align-items-center"> {{$promotions->name}}</p>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    @include('includes.footer')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <a href="{{$pageConfiguration->whatsapp_api}}&text=Halo%20Suryaputra%21%20Mau%20nanya%20informasi%20lebih%20lanjut%20nih." class="float" target="_blank">
        <i class="fa fa-whatsapp my-float"></i>
    </a>
    <!-- Modal -->
    <div class="modal fade custom_search_pop" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="serch_form">
                    <input type="text" placeholder="Search">
                    <button type="submit">search</button>
                </div>
            </div>
        </div>
    </div>
    <!-- link that opens popup -->
    <!-- JS here -->
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/ajax-form.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/scrollIt.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/jquery-ui.min.js"> </script>
    <script src="js/nice-select.min.js"></script>
    <script src="js/jquery.slicknav.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/range.js"></script>
    <!-- <script src="js/gijgo.min.js"></script> -->
    <script src="js/slick.min.js"></script>
    <script src="{{ asset('racks/js/navigation.js')}}"></script>
    <!--contact js-->
    <script src="js/contact.js"></script>
    <script src="js/jquery.ajaxchimp.min.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/mail-script.js"></script>
    <script src="js/main.js"></script>
    <script>
        $('#datepicker').datepicker({
            iconsLibrary: 'fontawesome',
            icons: {
                rightIcon: '<span class="fa fa-caret-down"></span>'
            }
        });
    </script>
    </script>
</body>

</html>