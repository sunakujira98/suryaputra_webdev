<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Suryaputra Bus Pariwisata - Beranda</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- <link rel="manifest" href="site.webmanifest"> -->
    <link rel="shortcut icon" type="image/x-icon" href="img/logo4.png">
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/themify-icons.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/gijgo.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/slicknav.css">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="css/style.css">
    <!-- <link rel="stylesheet" href="css/responsive.css"> -->
    <style>
    .float{
        position:fixed;
        width:60px;
        height:60px;
        bottom:40px;
        right:40px;
        background-color:#25d366;
        color:#FFF;
        border-radius:50px;
        text-align:center;
    font-size:30px;
        box-shadow: 2px 2px 3px #999;
    z-index:100;
    }

    .my-float{
        margin-top:16px;
    }
    </style>
</head>

<body>
    <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

    @include('includes.header')

    <!-- slider_area_start -->
    <div class="slider_area">
        <div class="slider_active owl-carousel">
            <div class="single_slider  d-flex align-items-center slider_bg_1 overlay">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-xl-12 col-md-12">
                            <div class="slider_text text-center">
                                <h3>Suryaputra</h3>
                                <p>Jasa Penyewaan Bus Pariwisata</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- slider_area_end -->

    <!-- where_togo_area_start  -->
    <!-- <div class="where_togo_area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <div class="form_area">
                        <h3>Where you want to go?</h3>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="search_wrap">
                        <form class="search_form" action="#">
                            <div class="input_field">
                                <input type="text" placeholder="Where to go?">
                            </div>
                            <div class="input_field">
                                <input id="datepicker" placeholder="Date">
                            </div>
                            <div class="input_field">
                                <select>
                                    <option data-display="Travel type">Travel type</option>
                                    <option value="1">Some option</option>
                                    <option value="2">Another option</option>
                                </select>
                            </div>
                            <div class="search_btn">
                                <button class="boxed-btn4 " type="submit" >Search</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- where_togo_area_end  -->

    <div class="container" style="margin-top:5%;">
        @include('includes.flash-message')
        <div class="section-tittle text-center">
            <span class="element">Promo</span>
        </div>
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">

            <li data-target="#myCarousel" data-slide-to="{{$lastPromotion->id}}" class="active"></li>
            @foreach($somePromotion as $promotions)
                <li data-target="#myCarousel" data-slide-to="{{$promotions->id}}"></li>
            @endforeach
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                <a data-target="#promotion-{{$lastPromotion->id}}" data-toggle="modal" class="MainNavText" id="MainNavHelp"  href="#myModal">
                    <img src="{{'https://drive.google.com/uc?export=view&id='.$lastPromotion->image_url}}" alt="Los Angeles" style="width:100%;">
                </a>
                </div>

                @foreach($somePromotion as $promotions)
                <div class="item">
                <a data-target="#promotion-{{$promotions->id}}" data-toggle="modal" class="MainNavText" id="MainNavHelp"  href="#myModal">
                    <img src="{{'https://drive.google.com/uc?export=view&id='.$promotions->image_url}}" alt="Los Angeles" style="width:100%;">
                </a>
                </div>
                @endforeach
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    
    
    <!-- popular_destination_area_start  -->
    <div class="popular_destination_area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section_title text-center mb_70">
                        <h3>Selamat Datang Di Suryaputra</h3>
                        <p>Selamat datang di Website Resmi Perusahaan Bus Suryaputra. Buka dan nikmati setiap detail menu-menu di web ini untuk mendapatkan informasi tentang perusahaan, leisure, kepuasan pelanggan, armada dari sebuah layanan penyewaan bus pariwisata.</p>
                    </div>
                </div>
            </div>
            <!-- <div class="row">
                @foreach($bus as $buses)
                <div class="col-lg-4 col-md-6">
                    <div class="single_destination">
                        <div class="thumb" style="max-height:230px;">
                            @php
                                $hypehenSentece = \Util::hypehenize($buses->name);
                            @endphp
                            <a href="{{ url('/busdetail-'.$buses->id.'-'.$hypehenSentece)}}"> <img src="https://drive.google.com/uc?export=view&id={{$buses->image_url}}" alt="{{$buses->name}}"> </a>
                        </div>
                        <div class="content">
                            @php
                                $hypehenSentece = \Util::hypehenize($buses->name);
                            @endphp
                            <p class="d-flex align-items-center">{{$buses->name}} <a href="{{ url('/busdetail-'.$buses->id.'-'.$hypehenSentece)}}">  {{$buses->number_of_seats . ' seats'}}</a> </p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div> -->

            <div class="section-tittle text-center">
            <!-- <span class="element">Promo</span> -->
        </div>

        <div id="myCarousel2" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">

            <li data-target="#myCarousel2" data-slide-to="armada-slider-{{$firstBus[0]->id}}" class="active"></li>
            <li data-target="#myCarousel2" data-slide-to="armada-slider-{{$secondBus[0]->id}}"></li>
            <li data-target="#myCarousel2" data-slide-to="armada-slider-{{$thirdBus[0]->id}}"></li>
            <li data-target="#myCarousel2" data-slide-to="armada-slider-{{$lastBus[0]->id}}"></li>

            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <div class="row">
                        @foreach($firstBus as $fbus)
                            @php
                                $hypehenSentece = \Util::hypehenize($fbus->name);
                            @endphp
                            <div class="col-md-4">
                                <div class="single_destination">
                                    <div class="thumb" style="max-height:230px;">
                                        <a href="{{ url('/busdetail-'.$fbus->id.'-'.$hypehenSentece)}}"> <img src="https://drive.google.com/uc?export=view&id={{$fbus->image_url}}" alt="{{$fbus->name}}"></a>
                                    </div>
                                    <div class="content">
                                        @php
                                            $hypehenSentece = \Util::hypehenize($fbus->name);
                                        @endphp
                                        <p class="d-flex align-items-center">{{$fbus->name}} <a href="{{ url('/busdetail-'.$fbus->id.'-'.$hypehenSentece)}}">  {{$fbus->number_of_seats . ' seats'}}</a> </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
        
                <div class="item">
                    <div class="row">
                    @foreach($thirdBus as $tbus)
                        @php
                            $hypehenSentece = \Util::hypehenize($tbus->name);
                        @endphp
                        <div class="col-md-4">
                            <div class="single_destination">
                                <div class="thumb" style="max-height:230px;">
                                    <a href="{{ url('/busdetail-'.$tbus->id.'-'.$hypehenSentece)}}"> <img src="https://drive.google.com/uc?export=view&id={{$tbus->image_url}}" alt="{{$tbus->name}}"></a>
                                </div>
                                <div class="content">
                                    @php
                                        $hypehenSentece = \Util::hypehenize($tbus->name);
                                    @endphp
                                    <p class="d-flex align-items-center">{{$tbus->name }} <a href="{{ url('/busdetail-'.$tbus->id.'-'.$hypehenSentece)}}">  {{$tbus->number_of_seats . ' seats'}}</a> </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>

                <div class="item">
                    <div class="row">
                    @foreach($thirdBus as $tbus)
                        @php
                            $hypehenSentece = \Util::hypehenize($tbus->name);
                        @endphp
                        <div class="col-md-4">
                            <div class="single_destination">
                                <div class="thumb" style="max-height:230px;">
                                    <a href="{{ url('/busdetail-'.$tbus->id.'-'.$hypehenSentece)}}"> <img src="https://drive.google.com/uc?export=view&id={{$tbus->image_url}}" alt="{{$tbus->name}}"></a>
                                </div>
                                <div class="content">
                                    @php
                                        $hypehenSentece = \Util::hypehenize($tbus->name);
                                    @endphp
                                    <p class="d-flex align-items-center">{{$tbus->name }} <a href="{{ url('/busdetail-'.$tbus->id.'-'.$hypehenSentece)}}">  {{$tbus->number_of_seats . ' seats'}}</a> </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>

                <div class="item">
                    <div class="row">
                    @foreach($lastBus as $lbus)
                        @php
                            $hypehenSentece = \Util::hypehenize($lbus->name);
                        @endphp
                        <div class="col-md-4">
                            <div class="single_destination">
                                <div class="thumb" style="max-height:230px;">
                                    <a href="{{ url('/busdetail-'.$lbus->id.'-'.$hypehenSentece)}}"> <img src="https://drive.google.com/uc?export=view&id={{$lbus->image_url}}" alt="{{$lbus->name}}"></a>
                                </div>
                                <div class="content">
                                    @php
                                        $hypehenSentece = \Util::hypehenize($lbus->name);
                                    @endphp
                                    <p class="d-flex align-items-center">{{$lbus->name}} <a href="{{ url('/busdetail-'.$lbus->id.'-'.$hypehenSentece)}}">  {{$lbus->number_of_seats . ' seats'}}</a> </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>

            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel2" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel2" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
            </a>
        </div>
        </div>
    </div>
    <!-- popular_destination_area_end  -->

    <!-- popular_destination_area_start  -->
    <div class="popular_destination_area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section_title text-center mb_70">
                        <h3>Selamat Datang Di Suryaputra</h3>
                        <p>Selamat datang di Website Resmi Perusahaan Bus Suryaputra. Buka dan nikmati setiap detail menu-menu di web ini untuk mendapatkan informasi tentang perusahaan, leisure, kepuasan pelanggan, armada dari sebuah layanan penyewaan bus pariwisata.</p>
                    </div>
                </div>
            </div>
            <!-- <div class="row">
                @foreach($bus as $buses)
                <div class="col-lg-4 col-md-6">
                    <div class="single_destination">
                        <div class="thumb" style="max-height:230px;">
                            @php
                                $hypehenSentece = \Util::hypehenize($buses->name);
                            @endphp
                            <a href="{{ url('/busdetail-'.$buses->id.'-'.$hypehenSentece)}}"> <img src="https://drive.google.com/uc?export=view&id={{$buses->image_url}}" alt="{{$buses->name}}"> </a>
                        </div>
                        <div class="content">
                            @php
                                $hypehenSentece = \Util::hypehenize($buses->name);
                            @endphp
                            <p class="d-flex align-items-center">{{$buses->name}} <a href="{{ url('/busdetail-'.$buses->id.'-'.$hypehenSentece)}}">  {{$buses->number_of_seats . ' seats'}}</a> </p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div> -->

            <div class="section-tittle text-center">
            <!-- <span class="element">Promo</span> -->
        </div>

        <div id="myCarousel2" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel2" data-slide-to="armada-slider-{{$firstBus[0]->id}}" class="active"></li>
                <li data-target="#myCarousel2" data-slide-to="armada-slider-{{$secondBus[0]->id}}"></li>
                <li data-target="#myCarousel2" data-slide-to="armada-slider-{{$thirdBus[0]->id}}"></li>
                <li data-target="#myCarousel2" data-slide-to="armada-slider-{{$lastBus[0]->id}}"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <div class="row">
                        @foreach($firstBus as $fbus)
                            @php
                                $hypehenSentece = \Util::hypehenize($fbus->name);
                            @endphp
                            <div class="col-md-4">
                                <div class="single_destination">
                                    <div class="thumb" style="max-height:230px;">
                                        <a href="{{ url('/busdetail-'.$fbus->id.'-'.$hypehenSentece)}}"> <img src="https://drive.google.com/uc?export=view&id={{$fbus->image_url}}" alt="{{$fbus->name}}"></a>
                                    </div>
                                    <div class="content">
                                        @php
                                            $hypehenSentece = \Util::hypehenize($fbus->name);
                                        @endphp
                                        <p class="d-flex align-items-center">{{$fbus->name}} <a href="{{ url('/busdetail-'.$fbus->id.'-'.$hypehenSentece)}}">  {{$fbus->number_of_seats . ' seats'}}</a> </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            </a>
        </div>
        </div>
    </div>
    <!-- popular_destination_area_end  -->

    <!-- newletter_area_start  -->
    <div class="newletter_area overlay">
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-lg-10">
                    <div class="row align-items-center">
                        <div class="col-lg-5">
                            <div class="newsletter_text">
                                <h4>Subscribe Newsletter Kita</h4>
                                <p>Subscribe sekarang ke website kita untuk selalu mendapatkan informasi terkini dari Suryaputra.</p>
                            </div>
                        </div>
                        <div class="col-lg-7">
                        <form action="{{ route('subscribe')}}" method="post">
                        @csrf
                            <div class="mail_form">
                                <div class="row no-gutters">
                                    <div class="col-lg-9 col-md-8">
                                        <div class="newsletter_field">
                                            <input type="email" placeholder="Masukkan email anda" name="email" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4">
                                        <div class="newsletter_btn">
                                            <button class="boxed-btn4 " type="submit" >Subscribe</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- newletter_area_end  -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <a href="{{$pageConfiguration->whatsapp_api}}&text=Halo%20Suryaputra%21%20Mau%20nanya%20informasi%20lebih%20lanjut%20nih." class="float" target="_blank">
    <i class="fa fa-whatsapp my-float"></i>
    </a>

    <div class="travel_variation_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="single_travel text-center">
                        <div class="icon">
                            <img src="img/svg_icon/transport_color.png" alt="">
                        </div>
                        <h3>Comfortable Journey</h3>
                        <p>Suryaputra Bus Pariwisata siap menemani perjalanan Anda setiap saat, dimanapun dan kapanpun dengan menyediakan pelayanan yang memuaskan.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single_travel text-center">
                        <div class="icon">
                            <img src="img/svg_icon/hydroalcoholic-gel_color.png" alt="">
                        </div>
                        <h3>Health</h3>
                        <p>Suryaputra Bus Pariwisata mengikuti standar protokol yang diterapkan pemerintah dalam mencegah terjadinya penyebaran Covid-19, Kami siap menemani perjalanan Anda dengan standar kesehatan yang tinggi.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single_travel text-center">
                        <div class="icon">
                            <img src="img/svg_icon/loyalty_color.png" alt="">
                        </div>
                        <h3>Care</h3>
                        <p>Jangan ragu untuk memilih kami sebagai partner karyawisata anda, karena kami memberikan kepedulian yang baik kepada Team dan Partner Kami.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="recent_trip_area" style="padding-bottom:0px; padding-top:0px;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section_title text-center mb_70">
                        <h3>Artikel Terbaru</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($article as $articles)
                <div class="col-lg-4 col-md-6">
                    <div class="single_trip">
                        <div class="thumb">
                            <img src="{{$articles->image_url}}" alt="">
                        </div>
                        <div class="info">
                            <div class="date">
                                <span>{{$articles->created_at->format('d M Y')}}</span>
                            </div>
                            <a href="#">
                                <h3>{{$articles->title}}</h3>
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    @include('includes.footer')
    
    <div class="modal fade" id="promotion-{{$lastPromotion->id}}">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Keterangan Promosi</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <img src="{{'https://drive.google.com/uc?export=view&id='.$lastPromotion->image_url}}" alt="Los Angeles" style="width:100%;">
                    </div>
                </div>

                <div class="col-md-12">
                <div class="form-group">
                    <label>Syarat & Ketentuan</label>
                    {!! $lastPromotion->data !!}
                </div>
                </div>
            </div>
        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    @foreach($somePromotion as $promotions)
    <div class="modal fade" id="promotion-{{$promotions->id}}">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Keterangan promosi</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <img src="{{'https://drive.google.com/uc?export=view&id='.$lastPromotion->image_url}}" alt="Los Angeles" style="width:100%;">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label>Syarat & Ketentuan</label>
                        {!! $promotions->data !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    @endforeach

  <!-- Modal -->
  <div class="modal fade custom_search_pop" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="serch_form">
            <input type="text" placeholder="Search" >
            <button type="submit">search</button>
        </div>
      </div>
    </div>
  </div>
    <!-- link that opens popup -->
<!--     
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://static.codepen.io/assets/common/stopExecutionOnTimeout-de7e2ef6bfefd24b79a3f68b414b87b8db5b08439cac3f1012092b2290c719cd.js"></script>

    <script src=" https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"> </script> -->
    <!-- JS here -->
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/ajax-form.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/scrollIt.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/nice-select.min.js"></script>
    <script src="js/jquery.slicknav.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/gijgo.min.js"></script>
    <script src="js/slick.min.js"></script>
   

    
    <!--contact js-->
    <script src="js/contact.js"></script>
    <script src="js/jquery.ajaxchimp.min.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/mail-script.js"></script>


    <script src="js/main.js"></script>
    <script>
        $('#datepicker').datepicker({
            iconsLibrary: 'fontawesome',
            icons: {
             rightIcon: '<span class="fa fa-caret-down"></span>'
         }
        });
    </script>
</body>

</html>