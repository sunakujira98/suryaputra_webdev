<!doctype html>
<html class="no-js" lang="zxx">

<head>
   <meta charset="utf-8">
   <meta http-equiv="x-ua-compatible" content="ie=edge">
   <title>Suryaputra Bus Pariwisata - Artikel</title>
   <meta name="description" content="">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   @include('includes.meta')
   @include('includes.meta-pages')
   <link rel="shortcut icon" type="image/x-icon" href="img/logo4.png">
   <!-- Place favicon.ico in the root directory -->
   <!-- CSS here -->
   <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" href="{{ asset('fonts/kanitsemibold/stylesheet.css')}}">
   <link rel="stylesheet" href="css/owl.carousel.min.css">
   <link rel="stylesheet" href="css/magnific-popup.css">
   <link rel="stylesheet" href="css/font-awesome.min.css">
   <link rel="stylesheet" href="css/themify-icons.css">
   <link rel="stylesheet" href="css/nice-select.css">
   <link rel="stylesheet" href="css/flaticon.css">
   <link rel="stylesheet" href="css/gijgo.css">
   <link rel="stylesheet" href="css/animate.css">
   <link rel="stylesheet" href="css/slicknav.css">
   <link rel="stylesheet" href="css/style.css">
   <link rel="stylesheet" href="{{ asset('fonts/ppwoodland/stylesheet.css')}}">
   <link rel="stylesheet" href="{{ asset('racks/css/style.css')}}">
   <link rel="stylesheet" href="{{ asset('racks/css/custom.css')}}">
</head>

<body>
   @include('includes.header')
   <!-- Banner-->
   <div class="site-blocks-cover overlay background-dynamic" data-aos="fade" id="home-section">
      <div class="banner-title">
         <h1 class="text color-responsive" data-aos="fade-up">Artikel / Blog</h1>
         <hr class="text-center hr-color">
      </div>
   </div>
   <img src=" {{url('img/block.PNG')}}" width="100%;" height="50px;">

   <!--================Blog Area =================-->
   <section class="blog_area section-padding">
      <div class="container">
         <div class="row">
            <div class="col-lg-8 mb-5 mb-lg-0">
               <div class="blog_left_sidebar">
                  @foreach($article as $articles)
                  <article class="blog_item">
                     <div class="blog_item_img">
                        <img class="card-img rounded-0" src="{{$articles->image_url}}" alt="">
                        <a href="#" class="blog_item_date">
                           <h3>{{$articles->created_at->format('d')}}</h3>
                           <p>{{$articles->created_at->format('M')}}</p>
                        </a>
                     </div>
                     <div class="blog_details">
                        <a href="{{ url('/blogdetail-'.$articles->id.'-'.$articles->slug)}}" class="d-inline-block">
                           <h2 style="font-family: Arial; font-size: 22px; font-style: normal; font-variant: normal; font-weight: 700;color:#328953;">{{$articles->title}}</h2>
                        </a>
                        <img src="{{'https://drive.google.com/uc?export=view&id='.$articles->image_url}}" style="max-width:90%">
                        <p>{!! $articles->opening !!}</p>
                        <ul class="blog-info-link">
                           <li><a href="#"><i class="fa fa-user"></i> {{$articles->category->name}}</a></li>
                           <!-- <li><a href="#"><i class="fa fa-comments"></i> 03 Comments</a></li> -->
                        </ul>
                     </div>
                  </article>
                  @endforeach
               </div>
               {{$article->links()}}
            </div>
            <div class="col-lg-4">
               <div class="blog_right_sidebar">
                  <aside class="single_sidebar_widget search_widget">
                     <form action="#">
                        <div class="form-group">
                           <div class="input-group mb-3">
                              <input type="text" class="form-control" placeholder='Search Keyword' onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search Keyword'">
                              <div class="input-group-append">
                                 <button class="btn" type="button"><i class="ti-search"></i></button>
                              </div>
                           </div>
                        </div>
                        <button class="button rounded-0 primary-bg text-white w-100 btn_1 boxed-btn" type="submit">Search</button>
                     </form>
                  </aside>
                  <aside class="single_sidebar_widget post_category_widget">
                     <h4 class="widget_title">Category</h4>
                     <ul class="list cat-list">
                        @foreach($category as $categories)
                        <li>
                           <a href="#" class="d-flex">
                              <p>{{$categories->name}}</p>
                           </a>
                        </li>
                        @endforeach
                     </ul>
                  </aside>
                  <aside class="single_sidebar_widget popular_post_widget">
                     <h3 class="widget_title">Artikel Terbaru</h3>
                     @foreach($recentArticle as $recents)
                     <div class="media post_item">
                        <img src="{{'https://drive.google.com/uc?export=view&id='.$recents->image_url}}" alt="post" style="max-width:80px;">
                        <div class="media-body">
                           <a href="{{ url('/blogdetail-'.$recents->id.'-'.$recents->slug)}}">
                              <h3>{{$recents->title}}</h3>
                           </a>
                           <p>{{$recents->created_at->format('d M Y')}}</p>
                        </div>
                     </div>
                     @endforeach
                  </aside>
                  <aside class="single_sidebar_widget newsletter_widget">
                     <h4 class="widget_title">Newsletter</h4>
                     <form action="#">
                        <div class="form-group">
                           <input type="email" class="form-control" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email'" placeholder='Enter email' required>
                        </div>
                        <button class="button rounded-0 primary-bg text-white w-100 btn_1 boxed-btn" type="submit">Subscribe</button>
                     </form>
                  </aside>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!--================Blog Area =================-->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
   <a href="{{$pageConfiguration->whatsapp_api}}&text=Halo%20Suryaputra%21%20Mau%20nanya%20informasi%20lebih%20lanjut%20nih." class="float" target="_blank">
      <i class="fa fa-whatsapp my-float"></i>
   </a>
   @include('includes.footer')
   <!--/ footer end  -->
   <!-- Modal -->
   <div class="modal fade custom_search_pop" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
            <div class="serch_form">
               <input type="text" placeholder="Search">
               <button type="submit">search</button>
            </div>
         </div>
      </div>
   </div>
   <!-- JS here -->
   <script src="js/vendor/modernizr-3.5.0.min.js"></script>
   <script src="js/vendor/jquery-1.12.4.min.js"></script>
   <script src="js/popper.min.js"></script>
   <script src="js/bootstrap.min.js"></script>
   <script src="js/owl.carousel.min.js"></script>
   <script src="js/isotope.pkgd.min.js"></script>
   <script src="js/ajax-form.js"></script>
   <script src="js/waypoints.min.js"></script>
   <script src="js/jquery.counterup.min.js"></script>
   <script src="js/imagesloaded.pkgd.min.js"></script>
   <script src="js/scrollIt.js"></script>
   <script src="js/jquery.scrollUp.min.js"></script>
   <script src="js/wow.min.js"></script>
   <script src="js/nice-select.min.js"></script>
   <script src="js/jquery.slicknav.min.js"></script>
   <script src="js/jquery.magnific-popup.min.js"></script>
   <script src="js/plugins.js"></script>
   <script src="js/gijgo.min.js"></script>
   <script src="{{ asset('racks/js/navigation.js')}}"></script>
   <!--contact js-->
   <script src="js/contact.js"></script>
   <script src="js/jquery.ajaxchimp.min.js"></script>
   <script src="js/jquery.form.js"></script>
   <script src="js/jquery.validate.min.js"></script>
   <script src="js/mail-script.js"></script>
   <script src="js/main.js"></script>
   <script>
      $('#datepicker').datepicker({
         iconsLibrary: 'fontawesome',
         icons: {
            rightIcon: '<span class="fa fa-caret-down"></span>'
         }
      });
      $('#datepicker2').datepicker({
         iconsLibrary: 'fontawesome',
         icons: {
            rightIcon: '<span class="fa fa-caret-down"></span>'
         }

      });
   </script>
</body>

</html>