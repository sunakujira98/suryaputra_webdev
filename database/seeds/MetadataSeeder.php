<?php

use Illuminate\Database\Seeder;

class MetadataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        date_default_timezone_set('Asia/Jakarta');
        //
        DB::table('metadata')->insert(
            [
                [
                    'page_name' => "seluruh_halaman",
                    'meta_name' => "keywords",
                    'meta_content' => 'Sewa bus pariwisata, bus pariwisata bandung,pariwisata bandung, elf bandung, sewa bus bandung pangandaran'
                ],
                [
                    'page_name' => "seluruh_halaman",
                    'meta_name' => "description",
                    'meta_content' => 'Bus pariwisata bandung. bus suryaputra untuk jelajahi berbagai destinasi wisata di indonesia.'
                ],
                [
                    'page_name' => "seluruh_halaman",
                    'meta_name' => "keywords",
                    'meta_content' => 'index,follow'
                ]
            ]
        );
    }
}
