<?php

use Illuminate\Database\Seeder;

class AllPagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        date_default_timezone_set('Asia/Jakarta');

        DB::table('all_pages')->insert(
            [
                [
                    'name' => "Seluruh Halaman",
                    'page_name' => "seluruh_halaman",
                ], [
                    'name' => "Beranda",
                    'page_name' => "beranda",
                ], [
                    'name' => "Tentang Kami",
                    'page_name' => "tentang_kami",
                ], [
                    'name' => "Armada",
                    'page_name' => "armada",
                ], [
                    'name' => "Promosi",
                    'page_name' => "promosi",
                ], [
                    'name' => "Artikel",
                    'page_name' => "artikel",
                ], [
                    'name' => "Kontak Kami",
                    'page_name' => "kontak_kami",
                ], [
                    'name' => "Blogdetail 4 Bus Pariwisata Suryaputra Menjadi Ofisial Transport Untuk Kegiatan Mojang Jajaka Kota Bandung",
                    'page_name' => "blogdetail-4-bus-pariwisata-suryaputra-menjadi-ofisial-transport-untuk-kegiatan-mojang-jajaka-kota-bandung"
                ], [
                    'name' => "busdetail 1 27 med executive plus",
                    'page_name' => "busdetail-1-27-med-executive-plus",
                ], [
                    'name' => "busdetail 2 27 medium premium",
                    'page_name' => "busdetail-2-27-medium-premium",
                ], [
                    'name' => "busdetail 3 29 medium deluxe",
                    'page_name' => "busdetail-3-29-medium-deluxe",
                ], [
                    'name' => "busdetail 4 29 medium executive",
                    'page_name' => "busdetail-4-29-medium-executive",
                ], [
                    'name' => "busdetail 5 47 seat deluxe",
                    'page_name' => "busdetail-5-47-seat-deluxe",
                ], [
                    'name' => "busdetail 6 47 seat premium",
                    'page_name' => "busdetail-6-47-seat-premium",
                ], [
                    'name' => "busdetail 7 avanza",
                    'page_name' => "busdetail-7-avanza",
                ], [
                    'name' => "busdetail 8 innova",
                    'page_name' => "busdetail-8-innova",
                ], [
                    'name' => "busdetail 9 commuter 14 seat",
                    'page_name' => "busdetail-9-commuter-14-seat",
                ], [
                    'name' => "busdetail 10 elf long 18 seats",
                    'page_name' => "busdetail-10-elf-long-18-seats",
                ], [
                    'name' => "busdetail 11 elf jumbo 18 seats",
                    'page_name' => "busdetail-11-elf-jumbo-18-seats",
                ]
            ]
        );
    }
}
