<div><img src="{{ asset('img/pelangi.png')}}" style="width:100%; position:relative; bottom:-10px;"></div>
<footer style="background-color:#3b528f;">
   <div class="container" style="color:white">
      <div class="row">
         <div class="col-md-3" style="font-size:12px">
            <img class="footer-logo image-responsive" src="{{ asset('img/logo3.png')}}">
            <div class="footer-address">
               Jalan Soekarno Hatta No. 269</br>
               Kebon Lega, Bojongloa Kidul,</br>
               Bandung City, West Java 40235</br>
               0812-1480-8888</br>
               bussuryaputra@gmail.com
            </div>
         </div>
         <div class="col-md-3 footer-column">
            <h6 style="color:white">TENTANG KAMI</h6>
            <ul class="list-unstyled">
               <li><a href="{{ url('/aboutus')}}">Tentang Kami</a></li>
            </ul>
         </div>
         <div class="col-md-3 footer-column">
            <h6 style="color:white">PRODUK & LAYANAN</h6>
            <ul class="list-unstyled">
               <li><a href="{{ url('/bus')}}">Armada</a></li>
            </ul>
         </div>
         <div class="col-md-3 footer-column">
            <h6 style="color:white">KONTAK KAMI</h6>
            <ul class="list-unstyled">
               <li><a href="{{ url('contact')}}">FAQ</a></li>
            </ul>
         </div>
      </div>
   </div>
</footer>