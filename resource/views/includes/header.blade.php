<header class="header-section">
   <div id="mySidenav" class="sidenav">
      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
      <a href="{{ url('/')}}">BERANDA</a>
      <a href="{{ url('/aboutus')}}">TENTANG KAMI</a>
      <a href="{{ url('/bus')}}">ARMADA</a>
      <a href="{{ url('/promotion')}}">PROMOSI</a>
      <a href="{{ url('blog')}}">ARTIKEL</a>
      <a href="{{ url('contact')}}">KONTAK KAMI</a>
   </div>
   <div class="header-warp">
      <div class="header-social d-flex justify-content-end">
         <span class="custom-navigation" onclick="openNav()">
            <i class="fa fa-bars fa-lg"></i>
         </span>
      </div>
      <!-- site logo -->
      <a href="{{ url('/')}}" class="banner-logo">
         <img src="{{ asset('img/logo_horizontal.png')}}" class="responsive-logo" />
      </a>
   </div>
</header>