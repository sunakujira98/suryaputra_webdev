<!DOCTYPE html>
<html lang="en">

<head>
   <title>Suryaputra Bus Pariwisata - Beranda</title>
   <link rel="shortcut icon" type="image/x-icon" href="img/logo4.png">
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <link rel="stylesheet" href="{{ asset('fonts/kanitsemibold/stylesheet.css')}}">
   <!-- <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Arial" /> -->
   <link rel="stylesheet" href="{{ asset('racks/css/animate.css')}}">
   <link rel="stylesheet" href="{{ asset('racks/css/owl.carousel.min.css')}}">
   <link rel="stylesheet" href="{{ asset('racks/css/owl.theme.default.min.css')}}">
   <link rel="stylesheet" href="{{ asset('racks/css/jquery.timepicker.css')}}">
   <link rel="stylesheet" href="css/font-awesome.min.css">
   <link rel="stylesheet" href="{{ asset('racks/css/style.css')}}">
   <link rel="stylesheet" href="{{ asset('racks/css/custom.css')}}">
   <!-- Stylesheets -->
   <link rel="stylesheet" href="{{ asset('racks/css/ionicons.min.css')}}">

   <link rel="stylesheet" href="{{ asset('racks/css/bootstrap-datepicker.css')}}">
   <link rel="stylesheet" href="{{ asset('plugins/lightbox/css/lightbox.css')}}">
   <style type="text/css">
      .gkKYBX {
         display: none !important;
      }
   </style>
</head>

<body>
   <!-- Header section -->
   @include('includes.header')
   <!-- Banner-->
   <div class="site-blocks-cover overlay background-dynamic" data-aos="fade" id="home-section">
      <div class="banner-title">
         <h3 class="text color-responsive" data-aos="fade-up">Jelajahi berbagai destinasi baru</h3>
         <h3 class="text color-responsive" data-aos="fade-up">Bersama Surya Putra</h3>
      </div>
   </div>
   <img src=" {{url('img/block.PNG')}}" width="100%;" height="50px;">

   <section class="ftco-section testimony-section">
      <div class="container">
         @include('includes.flash-message')
         <div class="row justify-content-center pb-5">
            <div class="col-md-7 text-center heading-section ftco-animate">
               <h2 style="font-family: Arial; font-size: 22px; font-style: normal; font-variant: normal; font-weight: 700;color:#328953;">PROMO BULAN INI</h2>
               <hr class="text-center" style="border-bottom: 4px solid #328953; width:80px; margin:auto">
            </div>
         </div>
         <div class="row ftco-animate">
            <div class="col-md-12">
               <div class="carousel-testimony owl-carousel ftco-owl">
                  @foreach($lastPromotion as $lastPromotions)
                  <div class="item text-center">
                     <div class="text">
                        <div class="user-img mb-4">
                           <span class="position" style="font-family: Arial; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 600; line-height: 20px; color:#3a5da6">{{$lastPromotions->name}}</span>
                        </div>
                        <div class="text">
                           <a data-target="#promotionl-{{$lastPromotions->id}}" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">
                              <img src="{{'https://drive.google.com/uc?export=view&id='.$lastPromotions->image_url}}" alt="Los Angeles" style="max-height:300px;">
                           </a>
                        </div>
                     </div>
                  </div>
                  @endforeach
                  @foreach($somePromotion as $somePromotions)
                  <div class="item text-center">
                     <div class="text">
                        <div class="user-img mb-4">
                           <span class="position" style="font-family: Arial; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 600; line-height: 20px; color:#3a5da6">{{$somePromotions->name}}</span>
                        </div>
                        <div class="text">
                           <a data-target="#promotions-{{$somePromotions->id}}" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">
                              <img src="{{'https://drive.google.com/uc?export=view&id='.$somePromotions->image_url}}" alt="Los Angeles" style="max-height:300px;">
                           </a>
                        </div>
                     </div>
                  </div>
                  @endforeach
               </div>
            </div>
            <div class="col-md-12" style="text-align:center;">
               <a class="btn btn-success" href="{{ url('/promotion')}}"> Lihat Semua Promo</a>
            </div>
         </div>
      </div>
   </section>
   <section class="ftco-section ftco-degree-bg">
      <div class="container newsletter-section">
         <div class="newsletter-content">
            <div class="row justify-content-center" style="margin-bottom:20px">
               <div class="col-md-7 text-center heading-section ftco-animate">
                  <h2 style="font-family: Arial; font-size: 20px; font-style: normal; font-variant: normal; line-height: 26.4px; color:#328953;">STAY UP TO DATE WITH OUR NEWSLETTER</h2>
               </div>
            </div>
            <form action="{{ route('subscribe')}}" method="post">
               <div class="row justify-content-center">
                  @csrf
                  <div class="col-md-6" style="text-align:right; padding:0px">
                     <input class="content-input" type="email" placeholder="Berlangganan Newsletter" name="email">
                  </div>
                  <div class="col-md-1" style="text-align:left; padding:0px">
                     <button class="content-button">SEND</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </section>
   <div id="google-reviews"></div>

   <section class="ftco-section ftco-degree-bg">
      <div class="container">
         <div class="row justify-content-center mb-2 pb-5">
            <div class="col-md-7 text-center heading-section ftco-animate">
               <h2 style="font-family: Arial; font-size: 22px; font-style: normal; font-variant: normal; font-weight: 700;color:#328953;">ALASAN UNTUK MEMILIH KAMI</h2>
               <hr class="text-center" style="border-bottom: 4px solid #328953; width:80px; margin:auto">
            </div>
         </div>
         <div class="row justify-content-center">
            <div class="col-md-4">
               <div class="media block-6 services d-block text-center">
                  <div class="d-flex justify-content-center">
                     <img src="img/svg_icon/bus-01_64.png" alt="">
                  </div>
                  <div class="media-body p-2">
                     <h3 class="heading" style="color:#3a5da6">Comfortable Journey</h3>
                     <p style="font-family: Arial; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 20px; color:#3a5da6">Menyediakan perjalanan yang aman untuk setiap penumpang kami, karena penumpang begitu berharga bagi kami.</p>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="media block-6 services d-block text-center">
                  <div class="d-flex justify-content-center">
                     <img src="img/svg_icon/care-01_64.png" alt="" style="height:66px;">
                  </div>
                  <div class="media-body p-2">
                     <h3 class="heading" style="color:#3a5da6">Care</h3>
                     <p style="font-family: Arial; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 20px; color:#3a5da6">Kami siap melayani Anda sepenuh hati. Mulai dari pemesanan armada hingga mencapai destinasi tujuan, melayani Anda dengan kualtias terbaik adalah prioritas kami.</p>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="media block-6 services d-block text-center">
                  <div class="d-flex justify-content-center">
                     <img src="img/svg_icon/shield-01-01.png" alt="" style="height:66px;">
                  </div>
                  <div class="media-body p-2">
                     <h3 class="heading" style="color:#3a5da6">Health & Safety</h3>
                     <p style="font-family: Arial; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 20px; color:#3a5da6">Kami memahami kekhawatiran Anda sehingga kami ingin Anda mengetahui bahwa setiap armada kamitelah dengan rutin di disinfeksi mengikuti protokol kesehatan nasional yang ada.</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="ftco-section testimony-section">
      <div class="container">
         <div class="row justify-content-center pb-5">
            <div class="col-md-7 text-center heading-section ftco-animate ">
               <h2 style="font-family: Arial; font-size: 22px; font-style: normal; font-variant: normal; font-weight: 700;color:#328953;">ARMADA KAMI</h2>
               <hr class="text-center" style="border-bottom: 4px solid #328953; width:80px; margin:auto">
            </div>
         </div>
         <div class="row ftco-animate">
            <div class="col-md-12">
               <div class="carousel-testimony owl-carousel ftco-owl">
                  @foreach($armada as $armadas)
                  <div class="item text-center">
                     <div class="text">
                        <div class="user-img mb-4">
                           <span class="position" style="font-family: Arial; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 600; line-height: 20px; color:#3a5da6">{{$armadas->name}}</span>
                        </div>
                        @php
                        $hypehenSentece = \Util::hypehenize($armadas->name);
                        @endphp
                        <a href="{{ url('/busdetail-'.$armadas->id.'-'.$hypehenSentece)}}">
                           <div class="text">
                              <img src="{{'https://drive.google.com/uc?export=view&id='.$armadas->image_url}}" alt="Masalah saat memuat gambar-{{$armadas->name}}" style="height:150px;">
                           </div>
                        </a>
                     </div>
                  </div>
                  @endforeach
               </div>
               <div class="col-md-12 mt-2" style="text-align:center;">
                  <a class="btn btn-success" href="{{ url('/bus')}}"> Lihat Semua Armada</a>
               </div>
            </div>
         </div>
      </div>
   </section>

   <section class="ftco-section-experience ftco-no-pb-experience ftco-no-pt-experience bg-primary-experience" style="margin-top:50px;">
      <div class="container">
         <div class="row justify-content-center">
            <div class="col-md-7 text-center mt-5">
               <h2 style="font-family: Arial; font-size: 22px; font-style: normal; font-variant: normal; font-weight: 700;color:#328953;">PENGALAMAN & PENGHARGAAN KAMI</h2>
               <hr class="text-center" style="border-bottom: 4px solid #328953; width:80px; margin:auto;margin-bottom: 40px;">
            </div>
         </div>
         <!-- <div class="row" style="padding-bottom:40px;">
            <div class="col-md-3" style="text-align:center">
               <a data-target="#pengalaman-1" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">
                  <img src="{{ asset('img/experience/400px-2018_Asian_Games_logo (1).png')}}" alt="" style="width:100px;height:130px">
               </a>
            </div>
            <div class="col-md-3" style="text-align:center">
               <a data-target="#pengalaman-2" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">
                  <img src="{{ asset('img/experience/wonderful indonesia (1).png')}}" alt="" style="width:100%; padding-top:20px">
               </a>
            </div>
            <div class="col-md-3" style="text-align:center">
               <a data-target="#pengalaman-3" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">
                  <img src="{{ asset('img/experience/pon jabar (1).png')}}" alt="" style="width:100%">
               </a>
            </div>
            <div class="col-md-3" style="text-align:center">
               <a data-target="#pengalaman-4" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">
                  <img src="{{ asset('img/experience/Logo-KAA (1).png')}}" alt="" style="width:120px;height:150px">
               </a>
            </div>
         </div>
         <div class="row" style="padding-bottom:40px;">
            <div class="col-md-3" style="text-align:center">
               <a data-target="#penghargaan-1" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">
                  <img src="{{ asset('img/achievement/achieve2.jpg')}}" alt="" style="width:170px; height:110px; margin-top:55px">
               </a>
            </div>
            <div class="col-md-3" style="text-align:center">
               <a data-target="#penghargaan-2" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">
                  <img src="{{ asset('img/achievement/achieve3.jpeg')}}" alt="" style="width:170px; height:110px; margin-top:55px">
               </a>
            </div>
            <div class="col-md-3" style="text-align:center">
               <a data-target="#penghargaan-3" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">
                  <img src="{{ asset('img/achievement/achieve4.png')}}" alt="" style="width:100%;">
               </a>
            </div>
            <div class="col-md-3" style="text-align:center">
               <a data-target="#penghargaan-4" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">
                  <img src="{{ asset('img/achievement/achieve1.jpeg')}}" alt="" style="width:100px; height:165px">
               </a>
            </div>
         </div> -->
         <div class="row ftco-animate">
            <div class="col-md-12">
               <div class="carousel-testimony owl-carousel ftco-owl">
                  <div class="item text-center">
                     <div class="text">
                           <a data-target="#pengalaman-1" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">
                              <img src="{{ asset('img/experience/400px-2018_Asian_Games_logo (1).png')}}" alt="" style="height:100px">
                           </a>
                     </div>
                  </div>
                  <div class="item text-center">
                        <a data-target="#pengalaman-2" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">
                           <img src="{{ asset('img/experience/wonderful indonesia (1).png')}}" alt="" style="height:100px">
                        </a>
                  </div>
                  <div class="item text-center">
                        <a data-target="#pengalaman-3" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">
                           <img src="{{ asset('img/experience/pon jabar (1).png')}}" alt="" style="height:100px">
                        </a>
                  </div>
                  <div class="item text-center">
                        <a data-target="#pengalaman-4" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">
                           <img src="{{ asset('img/experience/Logo-KAA (1).png')}}" alt="" style="height:100px">
                        </a>
                  </div>
                  <div class="item text-center">
                        <a data-target="#penghargaan-1" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">
                           <img src="{{ asset('img/achievement/achieve2.jpg')}}" alt="" style="height:100px">
                        </a>
                  </div>
                  <div class="item text-center">
                        <a data-target="#penghargaan-2" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">
                           <img src="{{ asset('img/achievement/achieve3.jpeg')}}" alt="" style="height:100px">
                        </a>
                  </div>
                  <div class="item text-center">
                        <a data-target="#penghargaan-2" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">
                           <img src="{{ asset('img/achievement/achieve4.png')}}" alt="" style="height:100px">
                        </a>
                  </div>
                  <div class="item text-center">
                        <a data-target="#penghargaan-2" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">
                           <img src="{{ asset('img/achievement/achieve1.jpeg')}}" alt="" style="height:100px">
                        </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section>
      <div class="container" style="margin-top:20px">
         <div class="row justify-content-center">
            <div class="col-md-7 text-center mt-5">
               <h2 style="font-family: Arial; font-size: 22px; font-style: normal; font-variant: normal; font-weight: 700;color:#328953;">APA KATA MEREKA ?</h2>
               <hr class="text-center" style="border-bottom: 4px solid #328953; width:80px; margin:auto">
            </div>
         </div>
         <script src="https://apps.elfsight.com/p/platform.js" defer></script>
         <div class="elfsight-app-464de4e4-7a78-4f78-a7f4-283a49347bce"></div>
      </div>
   </section>
   <section class="ftco-section ftco-degree-bg">
      <div class="container">
         <div class="row justify-content-center mb-5 pb-5">
            <div class="col-md-7 text-center heading-section ftco-animate">
               <h2 style="font-family: Arial; font-size: 22px; font-style: normal; font-variant: normal; font-weight: 700;color:#328953;">KLIK LINK SOSIAL MEDIA KAMI</h2>
               <hr class="text-center" style="border-bottom: 4px solid #328953; width:80px; margin:auto">
            </div>
         </div>
         <div class="row justify-content-center">
            <div class="col-md-2">
               <div class="media block-6 services d-block text-center">
                  <div class="d-flex justify-content-center">
                     <a href="{{$pageConfiguration->whatsapp_api}}&text=Halo%20Suryaputra%21%20Mau%20nanya%20informasi%20lebih%20lanjut%20nih." target="_blank"> <i class="fa fa-whatsapp fa-lg" style="color:green; font-size:50px; margin-bottom:15px"></i>
                     </a>
                  </div>
               </div>
            </div>
            <div class="col-md-2">
               <div class="media block-6 services d-block text-center">
                  <div class="d-flex justify-content-center">
                     <a href="{{$pageConfiguration->youtube_url}}" target="_blank">
                        <i class="fa fa-youtube-play fa-lg" style="color:green; font-size:50px; margin-bottom:15px"></i>
                     </a>
                  </div>
               </div>
            </div>
            <div class="col-md-2">
               <div class="media block-6 services d-block text-center">
                  <div class="d-flex justify-content-center">
                     <a href="{{$pageConfiguration->facebook_url}}" target="_blank">
                        <i class="fa fa-facebook fa-lg" style="color:green; font-size:50px; margin-bottom:15px"></i>
                     </a>
                  </div>
               </div>
            </div>
            <div class="col-md-2">
               <div class="media block-6 services d-block text-center">
                  <div class="d-flex justify-content-center">
                     <a href="{{$pageConfiguration->instagram_url}}" target="_blank">
                        <i class="fa fa-instagram fa-lg" style="color:green; font-size:50px; margin-bottom:15px"></i>
                     </a>
                  </div>
               </div>
            </div>
            <div class="col-md-2">
               <div class="media block-6 services d-block text-center">
                  <div class="d-flex justify-content-center">
                     <a href="{{$pageConfiguration->twitter_url}}" target="_blank">
                        <i class="fa fa-twitter fa-lg" style="color:green; font-size:50px; margin-bottom:15px"></i>
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <div class="recent_trip_area" style="padding-bottom:0px; padding-top:0px;">
      <div class="container">
         <div class="row justify-content-center" style="margin-bottom:50px">
            <div class="col-md-7 text-center heading-section ftco-animate">
               <h2 style="font-family: Arial; font-size: 22px; font-style: normal; font-variant: normal; font-weight: 700;color:#328953;">WHAT’S NEW ON SURYA PUTRA</h2>
               <hr class="text-center" style="border-bottom: 4px solid #328953; width:80px; margin:auto">
            </div>
         </div>
         <div class="row">
            @foreach($article as $articles)
            <div class="col-lg-4 col-md-6">
               <div class="single_trip">
                  <div class="thumb">
                     <img src="{{$articles->image_url}}" alt="">
                  </div>
                  <div class="info">
                     <div class="date">
                        <span>{{$articles->created_at->format('d M Y')}}</span>
                     </div>
                     <a href="{{ url('/blogdetail-'.$articles->id.'-'.$articles->slug)}}">
                        <h3 style="font-family: Arial; font-size: 22px; font-style: normal; font-variant: normal; font-weight: 700;color:#328953;">{{$articles->title}}</h3>
                     </a>
                  </div>
               </div>
            </div>
            @endforeach
         </div>
      </div>
   </div>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
   <a href="{{$pageConfiguration->whatsapp_api}}&text=Halo%20Suryaputra%21%20Mau%20nanya%20informasi%20lebih%20lanjut%20nih." class="float" target="_blank">
      <i class="fa fa-whatsapp my-float"></i>
   </a>
   <!-- footer -->
   @include('includes.footer')
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
   <a href="{{$pageConfiguration->whatsapp_api}}&text=Halo%20Suryaputra%21%20Mau%20nanya%20informasi%20lebih%20lanjut%20nih." class="float" target="_blank">
      <i class="fa fa-whatsapp my-float"></i>
   </a>
   <!-- loader -->
   <div id="ftco-loader" class="show fullscreen">
      <svg class="circular" width="48px" height="48px">
         <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
         <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" />
      </svg>
   </div>
   @foreach($somePromotion as $promotions)
   <div class="modal fade" id="promotions-{{$promotions->id}}">
      <div class="modal-dialog modal-xl">
         <div class="modal-content">
            <div class="modal-header">
               <h4 class="modal-title">Keterangan promosi</h4>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div class="modal-body">
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <img src="{{'https://drive.google.com/uc?export=view&id='.$promotions->image_url}}" alt="Los Angeles" style="width:100%;">
                     </div>
                  </div>

                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Syarat & Ketentuan</label>
                        {!! $promotions->data !!}
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal-footer justify-content-between">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </div>
         <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
   </div>
   <!-- /.modal -->
   @endforeach

   @foreach($lastPromotion as $promotionl)
   <div class="modal fade" id="promotionl-{{$promotionl->id}}">
      <div class="modal-dialog modal-xl">
         <div class="modal-content">
            <div class="modal-header">
               <h4 class="modal-title">Keterangan promosi</h4>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div class="modal-body">
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <img src="{{'https://drive.google.com/uc?export=view&id='.$promotionl->image_url}}" alt="Los Angeles" style="width:100%;">
                     </div>
                  </div>

                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Syarat & Ketentuan</label>
                        {!! $promotionl->data !!}
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal-footer justify-content-between">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </div>
         <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
   </div>
   <!-- /.modal -->
   @endforeach

   @for($i=1;$i<=4;$i++) @php if($i==1) { $image_url=asset('img/experience/400px-2018_Asian_Games_logo (1).png'); } else if($i==2) { $image_url=asset('img/experience/wonderful indonesia (1).png'); } else if($i==3) { $image_url=asset('img/experience/pon jabar (1).png'); } else { $image_url=asset('img/experience/Logo-KAA (1).png'); } @endphp <div class="modal fade" id="pengalaman-{{$i}}">
      <div class="modal-dialog modal-xl">
         <div class="modal-content">
            <div class="modal-header">
               <h4 class="modal-title">Keterangan Pengalaman</h4>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div class="modal-body">
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <img src="{{$image_url}}" alt="Detail Keterangan Penghargaan" style="display:block;margin-left:auto;margin-right:auto;width:80%;">
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal-footer justify-content-between">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </div>
         <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
      @endfor

      @for($i=1;$i<=4;$i++) @php if($i==1) { $image_url=asset('img/achievement/achieve2.jpg'); } else if($i==2) { $image_url=asset('img/achievement/achieve3.jpeg'); } else if($i==3) { $image_url=asset('img/achievement/achieve4.png'); } else { $image_url=asset('img/achievement/achieve1.jpeg'); } @endphp <div class="modal fade" id="penghargaan-{{$i}}">
         <div class="modal-dialog modal-xl">
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title">Keterangan Penghargaan</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <img src="{{$image_url}}" alt="Detail Keterangan Penghargaan" style="display:block;margin-left:auto;margin-right:auto;width:80%;">
                        </div>
                     </div>

                     <div class="col-md-12">
                        <div class="form-group" style="font-family: Arial; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 600;color:#328953;">
                           <label>
                              @if($i==1)
                              Dalam rangka mendukung penyelenggaraan mudik gratis oleh Kementrian Badan Usaha Milik Negara (BUMN) yang penyelenggaraannya diketuai oleh PT. Jasa Raharja (persero), PT. Suryaputra Anugerah menyediakan armada sebagai moda transportasi bagi para pemudik.
                              @elseif($i==2)
                              Dalam rangka mendukung upaya percepatan penanganan dan pencegahan Covid-19 di Jawa Barat, PT. Suryaputra Anugerah memberikan dukungan berupa fasilitas agar meningkatkan mobilitas para perawat Rumah Sakit Hasan Sadikin (RSHS).
                              @elseif($i==3)
                              Bus Pariwisata Suryaputra dinobatkan sebagai Perusahaan Angkutan Bus Terbaik pada tahun 2013 oleh Kementrian Perhubungan.
                              @else
                              International Organization for Standardization, atau lebih dikenal sebagai ISO, adalah salah satu standar internasional dalam sebuah sistem manajemen untuk pengukuran mutu organisasi. Atas kerjasama tim yang baik, Suryaputra berhasil mendapatkan sertifikasi ISO 9001:2015 pada tanggal 19 November 2019. Dengan tersertifikasi ISO, PT. Suryaputra Anugerah berkomitmen untuk terus mengembangkan SDM dan Sistem Manajemen demi memberikan konsumen pelayanan terbaik dan menjadi perusahaan jasa angkutan terbaik.
                              @endif
                           </label>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
            <!-- /.modal-content -->
         </div>
         <!-- /.modal-dialog -->
         </div>
         <!-- /.modal -->
         @endfor

         <script src="{{ asset('racks/js/jquery.min.js')}}"></script>
         <script src="{{ asset('racks/js/jquery-migrate-3.0.1.min.js')}}"></script>
         <script src="{{ asset('racks/js/popper.min.js')}}"></script>
         <script src="{{ asset('racks/js/bootstrap.min.js')}}"></script>
         <script src="{{ asset('racks/js/jquery.easing.1.3.js')}}"></script>
         <script src="{{ asset('racks/js/jquery.waypoints.min.js')}}"></script>
         <script src="{{ asset('racks/js/jquery.stellar.min.js')}}"></script>
         <script src="{{ asset('racks/js/owl.carousel.min.js')}}"></script>
         <script src="{{ asset('racks/js/jquery.magnific-popup.min.js')}}"></script>
         <script src="{{ asset('racks/js/aos.js')}}"></script>
         <script src="{{ asset('racks/js/navigation.js')}}"></script>
         <script src="{{ asset('plugins/lightbox/js/lightbox.js')}}"></script>
         <script src="racks/js/jquery.animateNumber.min.js"></script>
         <script src="racks/js/bootstrap-datepicker.js"></script>
         <script src="racks/js/jquery.timepicker.min.js"></script>
         <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
         <script src="racks/js/google-map.js"></script>
         <script src="racks/js/main.js"></script>
         <!--====== Javascripts & Jquery ======-->
         <script src="endgam/js/jquery.slicknav.min.js"></script>
         <script src="endgam/js/main.js"></script>
</body>

</html>