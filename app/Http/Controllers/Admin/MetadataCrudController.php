<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AllPages;
use Illuminate\Http\Request;
use App\Models\Metadata;

class MetadataCrudController extends Controller
{
    public function index()
    {
        $data['metadata'] = Metadata::all();
        $data['allPages'] = AllPages::orderBy('name')->get();

        return view('admin/metadata/index', $data);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'page_name' => 'required',
            'meta_name' => 'required',
            'meta_content' => 'required'
        ], [
            'page_name' => 'Page name harus diisi',
            'meta_name' => 'Meta name harus diisi',
            'meta_content' => 'Meta content harus diisi'
        ]);

        $metadata = new Metadata();
        $metadata->page_name = $request->get('page_name');
        $metadata->meta_name = $request->get('meta_name');
        $metadata->meta_content = $request->get('meta_content');
        $metadata->save();

        return \Redirect::back()->with(['success' => '<strong>Sukses!</strong> Berhasil menambah metadata pada page ' . $request->page_name]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'page_name' => 'required',
            'meta_name' => 'required',
            'meta_content' => 'required'
        ], [
            'page_name' => 'Page name harus diisi',
            'meta_name' => 'Meta name harus diisi',
            'meta_content' => 'Meta content harus diisi'
        ]);

        $metadata = Metadata::find($request->get('id'));
        $metadata->page_name = $request->get('page_name');
        $metadata->meta_name = $request->get('meta_name');
        $metadata->meta_content = $request->get('meta_content');
        $metadata->save();

        return \Redirect::back()->with(['success' => '<strong>Sukses!</strong> Berhasil mengubah metadata pada page ' . $request->page_name]);
    }

    public function destroy(Request $request)
    {
        $detailBus = Metadata::where('id', '=', $request->get('input_metadata_id'))->delete();

        return \Redirect::back()->with(['success' => '<strong>Sukses!</strong> Berhasil menghapus metadata']);
    }
}
