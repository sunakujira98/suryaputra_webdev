<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Article;
use App\Models\ArticleTags;
use App\Models\Category;
use App\Models\Tags;
use App\Helpers\Util;
use App\Models\AllPages;
use App\Models\Metadata;
use Carbon;

class ArticleCrudController extends Controller
{
    public function index()
    {
        $data['article'] = Article::all();
        $data['category'] = Category::all();
        $data['tag'] = Tags::all();
        $data['articleTags'] = ArticleTags::all();

        return view('admin/article.index', $data);
    }

    public function createForm()
    {
        $data['tags'] = Tags::all();
        $data['category'] = Category::all();

        return view('admin/article.create', $data);
    }

    public function create(Request $request)
    {
        $article = new Article();
        $article->category_id = $request->get('input_category');
        $article->title = $request->get('input_title');
        $article->image_url = $request->get('input_image_url');
        $article->slug = Util::slug($request->get('input_title'));
        $article->quote = $request->get('input_quote');
        $article->opening = $request->get('input_opening');
        $article->content = $request->get('input_content');
        $article->status = $request->get('input_status');
        $article->save();
        $article->tags()->attach($request->input_tags);
        $meta_content = "";
        foreach ($article->tags as $tags) {
            $meta_content .= $tags->name . ". ";
        }
        $article->save();

        $allPages = new AllPages();
        $allPages->name = 'blogdetail ' . $article->id . ' ' . $article->title;
        $allPages->page_name = 'blogdetail-' . $article->id . '-' . $article->slug;
        $allPages->save();

        $metadata = new Metadata();
        $metadata->page_name = 'blogdetail-' . $article->id . '-' . $article->slug;
        $metadata->meta_name = 'keywords';
        $metadata->meta_content = $meta_content;
        $metadata->save();

        return \Redirect::back()->with(['success' => '<strong>Sukses!</strong> Berhasil menambah artikel']);
    }

    public function update(Request $request)
    {
        $article = Article::findorfail($request->input_article_id);
        $article->category_id = $request->get('input_category');
        $article->title = $request->get('input_title');
        $article->image_url = $request->get('input_image_url');
        $article->slug = Util::slug($request->get('input_title'));
        $article->quote = $request->get('input_quote');
        $article->opening = $request->get('input_opening');
        $article->content = $request->get('input_content');
        $article->status = $request->get('input_status');
        $article->save();
        $article->tags()->attach($request->input_tags);
        $article->save();

        return \Redirect::back()->with(['success' => '<strong>Sukses!</strong> Berhasil merubah artikel']);
    }

    public function destroy(Request $request)
    {
        $articleTags = ArticleTags::where('article_id', '=', $request->get('input_article_id'))->delete();
        $article = Article::where('id', '=', $request->get('input_article_id'))->delete();

        return \Redirect::back()->with(['success' => '<strong>Sukses!</strong> Berhasil menghapus artikel']);
    }
}
