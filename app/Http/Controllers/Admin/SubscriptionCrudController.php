<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Member;
use Mail;

class SubscriptionCrudController extends Controller
{
    //

    public function index()
    {
        $data['member'] = Member::all();

        return view('admin/subscription/index', $data);
    }

    public function generate(Request $request)
    {
        $emailMember = Member::get('email');
        $pesan = $request->get('input_content');

        try {
            Mail::send('admin/email/email-newsletter', ['nama' => $request->nama, 'pesan' => $pesan, 'email' => $emailMember], function ($message) use ($request, $emailMember) {
                $message->subject($request->get('input_subject'));
                $message->from('stevenitsolution@gmail.com', 'Suryaputra Newsletter');
                foreach ($emailMember as $emailMember) {
                    $message->to($emailMember->email);
                }
            });
            return \Redirect::back()->with(['success' => '<strong>Terima Kasih</strong> telah subscribe newsletter Suryaputra Bus Pariwisata']);
        } catch (Exception $e) {
            return \Redirect::back()->with(['error' => '<strong>Mohon maaf</strong> Anda tidak dapat daftar kepada newsletter kita, hubungi kita melalu form yang tersedia pada Contact Us.']);
        }
    }

    public function destroy(Request $request)
    {
        $emailMember = Member::get('email');
        $pesan = $request->get('input_content');

        $member = Member::where('id', $request->get('input_member_id'))->delete();

        return \Redirect::back()->with(['success' => '<strong>Berhasil</strong> hapus member']);
    }

    public function create(Request $request)
    {
        $member = Member::where('email', '=', $request->get('email'))->get();
        if ($member->first()) {
            return \Redirect::back()->with(['info' => '<strong>Mohon maaf</strong> Anda sudah berlangganan pada newsletter kami :)']);
        } else {
            $member = new Member();
            $member->email = $request->get('email');
            $member->save();
            $pesan = "Makasih sudah subscribe yahh";
        }

        try {
            Mail::send('admin/email/email', ['nama' => $request->nama, 'pesan' => $pesan], function ($message) use ($request) {
                $message->subject("Terima Kasih Sudah Subscribe Suryaputra");
                $message->from('stevenitsolution@gmail.com', 'Suryaputra Newsletter');
                $message->to($request->email);
            });
            return \Redirect::back()->with(['success' => '<strong>Terima Kasih</strong> telah subscribe newsletter Suryaputra Bus Pariwisata']);
        } catch (Exception $e) {
            return \Redirect::back()->with(['error' => '<strong>Mohon maaf</strong> Anda tidak dapat daftar kepada newsletter kita, hubungi kita melalu form yang tersedia pada Contact Us.']);
        }
    }

    public function book(Request $request)
    {
        $bus = $request->get('input_bus_name');
        $telp = $request->get('input_telp');
        $email = $request->get('input_email');
        $pesan = "Ada yang tertarik sama bus" . $bus . ' dari website nih';

        try {
            Mail::send('admin/email/email-booking', ['nama' => $request->nama, 'pesan' => $pesan, 'telp' => $telp, 'email' => $email, 'bus' => $bus], function ($message) use ($request, $email) {
                $message->subject("Ada yang tertarik sama bus " . $request->get('input_bus_name') . "dari website");
                $message->from('stevenitsolution@gmail.com', 'Suryaputra Website Dev');
                $message->to('bussuryaputra@gmail.com');
            });
            return \Redirect::back()->with(['success' => '<strong>Terima Kasih</strong> telah membooking dari website kita!']);
        } catch (Exception $e) {
            return \Redirect::back()->with(['error' => '<strong>Mohon maaf</strong> Sistem booking sedang error.']);
        }
    }
}
