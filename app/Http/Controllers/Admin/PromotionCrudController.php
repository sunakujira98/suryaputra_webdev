<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Promotion;

class PromotionCrudController extends Controller
{
    //
    public function index() {
        $data['promotion'] = Promotion::all();

        return view('admin/promotion/index', $data);
    }

    public function create(Request $request) {
        $promotion = new Promotion();
        $promotion->image_url = $request->get('input_image_url');
        $promotion->data = $request->get('input_data');
        $promotion->save();

        return \Redirect::back()->with(['success' => '<strong>Sukses!</strong> Berhasil menambah promosi']);
    }

    public function update(Request $request) {
        $promotion = Promotion::findorfail($request->input_promotion_id);
        $promotion->image_url = $request->get('input_image_url');
        $promotion->data = $request->get('input_data');
        $promotion->save();

        return \Redirect::back()->with(['success' => '<strong>Sukses!</strong> Berhasil merubah promosi']);
    }

    public function destroy(Request $request) {
        $promotion = Promotion::where('id', '=', $request->get('input_promotion_id'))->delete();

        return \Redirect::back()->with(['success' => '<strong>Sukses!</strong> Berhasil menghapus promosi']);
    }

}
