<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Bus;
use App\Models\BusDetail;

class DetailBusCrudController extends Controller
{
    public function create(Request $request) {
        $this->validate($request, [
            'input_detailbus_name' => 'required',
            'input_detailbus_image_url' => 'required',
        ], [
            'input_detailbus_name' => 'Nama detailbus harus diisi',
            'input_detailbus_image_url' => 'Gambar detailbus harus diisi',
        ]);

        $detailBus = new BusDetail();
        $detailBus->bus_id = $request->get('input_detailbus_bus_id');
        $detailBus->name = $request->get('input_detailbus_name');
        $detailBus->image_url = $request->get('input_detailbus_image_url');
        $detailBus->save();

        return \Redirect::back()->with(['success' => '<strong>Sukses!</strong> Berhasil menambah data detailbus']);
    }

    public function update(Request $request) {
        $this->validate($request, [
            'input_detailbus_id' => 'required',
            'input_detailbus_name' => 'required',
            'input_detailbus_image_url' => 'required',
        ], [
            'input_detailbus_id' => 'ID detailbus tidak boleh kosong',
            'input_detailbus_name' => 'Nama detailbus harus diisi',
            'input_detailbus_image_url' => 'Gambar detailbus harus diisi',
        ]);

        $detailBus = BusDetail::find($request->get('input_detailbus_id'));
        $detailBus->name = $request->get('input_detailbus_name');
        $detailBus->image_url = $request->get('input_detailbus_image_url');
        $detailBus->save();

        return \Redirect::back()->with(['success' => '<strong>Sukses!</strong> Berhasil meng-update data detailbus']);
    }

    public function destroy(Request $request) {
        $detailBus = BusDetail::where('id', '=', $request->get('input_detailbus_id'))->delete();

        return \Redirect::back()->with(['success' => '<strong>Sukses!</strong> Berhasil menghapus data detailbus']);
    }
}
