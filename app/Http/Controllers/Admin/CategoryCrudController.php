<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;

class CategoryCrudController extends Controller
{
    //
    public function index() {
        $data['category'] = Category::all();

        return view('admin/category.index', $data);
    }

    public function create(Request $request) {
        $category = new Category();
        $category->name = $request->get('input_categories_name');
        $category->slug = $request->get('input_categories_slug');
        $category->save();

        return \Redirect::back()->with(['success' => '<strong>Sukses!</strong> Berhasil menambah category']);
    }

    public function update(Request $request) {
        $category = Category::findorfail($request->input_categories_id);
        $category->name = $request->get('input_categories_name');
        $category->slug = $request->get('input_categories_slug');
        $category->save();

        return \Redirect::back()->with(['success' => '<strong>Sukses!</strong> Berhasil merubah category']);
    }
}
