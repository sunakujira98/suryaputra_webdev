<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AllPages;
use App\Models\Bus;
use App\Models\BusDetail;
use App\Helpers\Util;
use Redirect;

class BusCrudController extends Controller
{
    //
    public function index()
    {
        $data['bus'] = Bus::all();

        return view('admin.index', $data);
    }

    public function show($id)
    {
        $data['busDetail'] = BusDetail::where('bus_id', '=', $id)->get();
        $data['bus'] = Bus::where('id', '=', $id)->first();

        return view('admin.bus.detail', $data);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'input_bus_name' => 'required',
            'input_bus_number_of_seats' => 'required',
            'input_bus_image_url' => 'required',
        ], [
            'input_bus_name' => 'Nama bus harus diisi',
            'input_bus_number_of_seats' => 'Jumlah tempat duduk harus diisi',
            'input_bus_image_url' => 'Gambar bus harus diisi',
        ]);

        $bus = new Bus();
        $bus->name = $request->get('input_bus_name');
        $bus->number_of_seats = $request->get('input_bus_number_of_seats');
        $bus->image_url = $request->get('input_bus_image_url');
        $bus->number_of_seats_description = $request->get('input_bus_number_of_seats_description');
        $bus->facility = $request->get('input_bus_facility');
        $bus->design = $request->get('input_bus_design');
        $bus->save();

        $allPages = new AllPages();
        $allPages->name = 'busdetail ' . $bus->id . ' ' . $bus->name;
        $allPages->page_name = 'busdetail-' . $bus->id . '-' . Util::slug($bus->name);
        $allPages->save();

        return redirect('/admin/data/bus')->with(['success' => '<strong>Sukses!</strong> Berhasil menambah data bus']);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'input_bus_id' => 'required',
            'input_bus_name' => 'required',
            'input_bus_number_of_seats' => 'required',
            'input_bus_image_url' => 'required',
            'input_bus_number_of_seats_description' => 'required',
            'input_bus_facility' => 'required',
            'input_bus_design' => 'required',
        ], [
            'input_bus_id' => 'ID bus tidak boleh kosong',
            'input_bus_name' => 'Nama bus harus diisi',
            'input_bus_number_of_seats' => 'Jumlah tempat duduk harus diisi',
            'input_bus_image_url' => 'Gambar bus harus diisi',
            'input_bus_number_of_seats_description' => 'Deskripsi tempat duduk bus harus diisi',
            'input_bus_facility' => 'Fasilitas bus harus diisi',
            'input_bus_design' => 'Design bus harus diisi',
        ]);

        $bus = Bus::find($request->get('input_bus_id'));
        $bus->name = $request->get('input_bus_name');
        $bus->number_of_seats = $request->get('input_bus_number_of_seats');
        $bus->image_url = $request->get('input_bus_image_url');
        $bus->number_of_seats_description = $request->get('input_bus_number_of_seats_description');
        $bus->facility = $request->get('input_bus_facility');
        $bus->design = $request->get('input_bus_design');
        $bus->save();

        return redirect('/admin/data/bus')->with(['success' => '<strong>Sukses!</strong> Berhasil meng-update data bus']);
    }

    public function destroy(Request $request)
    {
        $busDetail = BusDetail::where('bus_id', '=', $request->get('input_bus_id'))->delete();
        $bus = Bus::where('id', '=', $request->get('input_bus_id'))->delete();

        return \Redirect::back()->with(['success' => '<strong>Sukses!</strong> Berhasil menghapus data bus']);
    }
}
