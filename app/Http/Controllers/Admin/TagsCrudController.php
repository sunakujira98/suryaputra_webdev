<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tags;

class TagsCrudController extends Controller
{
    //
    public function index() {
        $data['tag'] = Tags::all();

        return view('admin/tags.index', $data);
    }

    public function create(Request $request) {
        $tags = new tags();
        $tags->name = $request->get('input_tags_name');
        $tags->slug = $request->get('input_tags_slug');
        $tags->save();

        return \Redirect::back()->with(['success' => '<strong>Sukses!</strong> Berhasil menambah tags']);
    }

    public function update(Request $request) {
        $tags = tags::findorfail($request->input_tags_id);
        $tags->name = $request->get('input_tags_name');
        $tags->slug = $request->get('input_tags_slug');
        $tags->save();

        return \Redirect::back()->with(['success' => '<strong>Sukses!</strong> Berhasil merubah tags']);
    }
}
