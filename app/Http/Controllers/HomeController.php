<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PageConfiguration;
use App\Models\Faq;
use App\Models\Article;
use App\Models\ArticleTags;
use App\Models\Bus;
use App\Models\BusDetail;
use App\Models\Category;
use App\Models\Metadata;
use App\Models\Promotion;
use Carbon\Carbon;

class HomeController extends Controller
{
    //
    protected function getPageConfiguration()
    {
        $pageConfiguration = PageConfiguration::first();

        return $pageConfiguration;
    }


    public function index()
    {
        $data['metaAllPages'] = Metadata::where('page_name', '=', 'seluruh_halaman')->get();
        $data['metaPages'] = Metadata::where('page_name', '=', 'beranda')->get();
        $data['pageConfiguration'] = Self::getPageConfiguration();
        $data['bus'] = Bus::orderBy('name')->get();
        $data['firstBus'] = Bus::orderBy('id', 'asc')->take(3)->get();
        $data['secondBus'] = Bus::orderBy('id', 'asc')->where('id', '>', 3)->take(3)->get();
        $data['thirdBus'] = Bus::orderBy('id', 'asc')->where('id', '>', 6)->take(3)->get();
        $data['lastBus'] = Bus::orderBy('id', 'asc')->where('id', '>', 9)->take(3)->get();
        $data['armada'] = Bus::all();

        $data['article'] = Article::orderBy('id', 'desc')->take(3)->get();
        $maxIdPromotion = Promotion::max('id');
        $data['lastPromotion'] = Promotion::orderBy('id', 'desc')->get();
        $data['somePromotion'] = Promotion::where('id', '<', $maxIdPromotion)->get();

        return view('welcome', $data);
    }

    public function aboutUsPage()
    {
        $data['metaAllPages'] = Metadata::where('page_name', '=', 'seluruh_halaman')->get();
        $data['metaPages'] = Metadata::where('page_name', '=', 'tentang_kami')->get();
        $data['pageConfiguration'] = Self::getPageConfiguration();
        $data['faq'] = Faq::all();
        $data['article'] = Article::orderBy('id', 'desc')->take(3)->get();

        return view('about', $data);
    }

    public function busPage()
    {
        $data['metaAllPages'] = Metadata::where('page_name', '=', 'seluruh_halaman')->get();
        $data['metaPages'] = Metadata::where('page_name', '=', 'armada')->get();
        $data['pageConfiguration'] = Self::getPageConfiguration();
        $data['bus'] = Bus::all();
        $data['article'] = Article::orderBy('id', 'desc')->take(3)->get();


        return view('bus', $data);
    }

    public function detailBusPage($id)
    {
        $current_url = basename($_SERVER['REQUEST_URI']);
        $getPageName = substr($current_url, strpos($current_url, "/"));
        $data['metaAllPages'] = Metadata::where('page_name', '=', 'seluruh_halaman')->get();
        $data['metaPages'] = Metadata::where('page_name', '=', $getPageName)->get();
        $data['bus'] = Bus::findorfail($id);
        $data['armada'] = Bus::where('id', '!=', $id)->get();
        $data['busDetail'] = BusDetail::where('bus_id', '=', $id)->get();
        $data['pageConfiguration'] = Self::getPageConfiguration();
        $data['article'] = Article::orderBy('id', 'desc')->take(3)->get();

        return view('busdetail', $data);
    }

    public function blogPage()
    {
        $data['metaAllPages'] = Metadata::where('page_name', '=', 'seluruh_halaman')->get();
        $data['metaPages'] = Metadata::where('page_name', '=', 'artikel')->get();
        $data['pageConfiguration'] = Self::getPageConfiguration();
        $data['article'] = Article::orderBy('created_at', 'desc')->where('status', 1)->paginate(3);
        $data['recentArticle'] = Article::orderBy('id', 'desc')->take(5)->get();
        $data['articleTags'] = ArticleTags::where('article_id', '=', 4)->get();
        $data['category'] = Category::all();
        return view('blog', $data);
    }

    public function blogDetailPage($id)
    {
        $current_url = basename($_SERVER['REQUEST_URI']);
        $getPageName = substr($current_url, strpos($current_url, "/"));
        $data['metaArticleDetail'] = Metadata::where('page_name', '=', $getPageName)->get();
        $data['metaAllPages'] = Metadata::where('page_name', '=', 'seluruh_halaman')->get();
        $data['pageConfiguration'] = Self::getPageConfiguration();
        $data['recentArticle'] = Article::orderBy('id', 'desc')->take(5)->get();
        $data['articleDetail'] = Article::where('id', '=', $id)->first();
        $data['articleTags'] = ArticleTags::where('article_id', '=', $id)->get();
        $data['category'] = Category::all();
        $maxId = Article::max('id');
        if ($id < $maxId) {
            $nextPage = $id + 1;
        } else {
            $nextPage = $id;
        }
        $prevPage = $id - 1;
        $data['nextPage'] = Article::where('id', '=', $nextPage)->first();
        $data['prevPage'] = Article::where('id', '=', $prevPage)->first();

        return view('blogdetail', $data);
    }

    public function promotionPage()
    {
        $data['metaAllPages'] = Metadata::where('page_name', '=', 'seluruh_halaman')->get();
        $data['metaPages'] = Metadata::where('page_name', '=', 'promosi')->get();
        $data['pageConfiguration'] = Self::getPageConfiguration();
        $data['promotion'] = Promotion::all();

        return view('promotion', $data);
    }

    public function detailPromotionPage($id)
    {
        $current_url = basename($_SERVER['REQUEST_URI']);
        $getPageName = substr($current_url, strpos($current_url, "/"));
        $data['metaAllPages'] = Metadata::where('page_name', '=', 'seluruh_halaman')->get();
        $data['metaPages'] = Metadata::where('page_name', '=', $getPageName)->get();
        $data['pageConfiguration'] = Self::getPageConfiguration();
        $data['promotion'] = Promotion::findorfail($id);

        return view('promotiondetail', $data);
    }

    public function contactPage()
    {
        $data['metaAllPages'] = Metadata::where('page_name', '=', 'seluruh_halaman')->get();
        $data['metaPages'] = Metadata::where('page_name', '=', 'kontak_kami')->get();
        $data['pageConfiguration'] = Self::getPageConfiguration();

        return view('contact', $data);
    }
}
