<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class ContactController extends Controller
{
    //

    public function create(Request $request)
    {
        $message = $request->get('message');
        $email = $request->get('email');
        $name = $request->get('name');

        try {
            Mail::send('admin/email/email-contactus', ['name' => $name, 'pesan' => $message, 'email' => $email], function ($message) use ($request) {
                $message->subject('Email Dari Contact US');
                $message->from('stevenitsolution@gmail.com', 'Suryaputra Newsletter');
                $message->to('bussuryaputra@gmail.com');
            });
            return \Redirect::back()->with(['success' => '<strong>Terima Kasih</strong> telah menghubungi email Suryaputra Bus Pariwisata']);
        } catch (Exception $e) {
            return \Redirect::back()->with(['error' => '<strong>Mohon maaf</strong> Anda tidak dapat daftar kepada newsletter kita, hubungi kita melalu form yang tersedia pada Contact Us.']);
        }
    }
}
