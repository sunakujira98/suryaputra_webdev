<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
    protected $table = 'article';
    protected $fillable = ['title', 'slug', 'quote', 'content', 'status', 'design'];

    function category() {
        return $this->belongsTo("App\Models\Category", "category_id", "id");
    }

    function tags() {
        return $this->belongsToMany("App\Models\Tags");
    }
}
