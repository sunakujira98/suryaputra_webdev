<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusDetail extends Model
{
    protected $table = 'bus_detail';
    protected $fillable = ['bus_id', 'image_url'];

    function bus() {
        return $this->belongsTo("App\Models\Bus", "bus_id", "id");
    }
}
