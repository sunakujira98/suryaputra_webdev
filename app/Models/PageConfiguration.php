<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageConfiguration extends Model
{
    protected $table = 'page_configuration';
    protected $fillable = ['email_address', 'whatsapp_number', 'whatapp_api', 'youtube_url', 'facebook_url', 'instagram_url'];
}
