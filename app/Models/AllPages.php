<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AllPages extends Model
{
    //
    protected $table = 'all_pages';
    protected $fillable = ['page_name', 'name'];
}
