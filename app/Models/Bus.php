<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bus extends Model
{
    //
    protected $table = 'bus';
    protected $fillable = ['name', 'number_of_seats', 'number_of_seats_description', 'facility', 'design'];

    function busDetail() {
        return $this->hasMany("App\Models\BusDetail", "bus_id", "id");
    }
}
