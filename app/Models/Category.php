<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';
    protected $fillable = ['name', 'slug'];

    function article() {
        return $this->hasMany("App\Models\Article", "category_id", "id");
    }
}
