<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "HomeController@Index");
Route::get('/aboutus', "HomeController@aboutUsPage");
Route::get('/bus', "HomeController@busPage");
Route::get('/busdetail-{id}-{name}', "HomeController@detailBusPage");
Route::get('/blog', "HomeController@blogPage");
Route::get('/blogdetail-{id}-{slug}', "HomeController@blogDetailPage");
Route::get('/contact', "HomeController@contactPage");
Route::get('/promotion', "HomeController@promotionPage");
Route::get('/promotion-{id}', "HomeController@detailPromotionPage");
Route::post('subscribe', "Admin\SubscriptionCrudController@create")->name('subscribe');
Route::post('/contact', "ContactController@create")->name('contact');
Route::post('book', "Admin\SubscriptionCrudController@book")->name('book');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/admin', "Admin\BusCrudController@index");
    Route::get('/admin/data/bus', [
        'uses' => 'Admin\BusCrudController@index',
        'as' => 'bus.index'
    ]);
    Route::get('/admin/data/bus/{id}', "Admin\BusCrudController@show");
    Route::post('update-bus', "Admin\BusCrudController@update")->name('admin.update-bus');
    Route::post('create-bus', "Admin\BusCrudController@create")->name('admin.create-bus');
    Route::post('delete-bus', "Admin\BusCrudController@destroy")->name('admin.delete-bus');

    Route::post('update-detailbus', "Admin\DetailBusCrudController@update")->name('admin.update-detailbus');
    Route::post('create-detailbus', "Admin\DetailBusCrudController@create")->name('admin.create-detailbus');
    Route::post('delete-detailbus', "Admin\DetailBusCrudController@destroy")->name('admin.delete-detailbus');

    Route::get('/admin/article/index', 'Admin\ArticleCrudController@index')->name('admin.index-article');
    Route::get('/admin/article/create', 'Admin\ArticleCrudController@createForm')->name('admin.create-article');
    Route::post('create-article', 'Admin\ArticleCrudController@create')->name('admin.create-article');
    Route::post('update-article', 'Admin\ArticleCrudController@update')->name('admin.update-article');
    Route::post('delete-article', "Admin\ArticleCrudController@destroy")->name('admin.delete-article');

    Route::get('/admin/category/index', 'Admin\CategoryCrudController@index')->name('admin.index-category');
    Route::post('create-category', 'Admin\CategoryCrudController@create')->name('admin.create-category');
    Route::post('update-category', 'Admin\CategoryCrudController@update')->name('admin.update-category');

    Route::get('/admin/tag/index', 'Admin\TagsCrudController@index')->name('admin.index-tag');
    Route::post('create-tag', 'Admin\TagsCrudController@create')->name('admin.create-tag');
    Route::post('update-tag', 'Admin\TagsCrudController@update')->name('admin.update-tag');

    Route::get('/admin/subscription/index', 'Admin\SubscriptionCrudController@index')->name('admin.index-subscription');
    Route::post('create-subscription', 'Admin\SubscriptionCrudController@generate')->name('admin.create-subscription');
    Route::post('delete-subscription', 'Admin\SubscriptionCrudController@destroy')->name('admin.delete-subscription');

    Route::get('/admin/promotion/index', 'Admin\PromotionCrudController@index')->name('admin.index-promotion');
    Route::post('create-promotion', 'Admin\PromotionCrudController@create')->name('admin.create-promotion');
    Route::post('update-promotion', 'Admin\PromotionCrudController@update')->name('admin.update-promotion');
    Route::post('delete-promotion', "Admin\PromotionCrudController@destroy")->name('admin.delete-promotion');

    Route::get('/admin/metadata/index', 'Admin\MetadataCrudController@index')->name('admin.index-metadata');
    Route::post('create-metadata', 'Admin\MetadataCrudController@create')->name('admin.create-metadata');
    Route::post('update-metadata', 'Admin\MetadataCrudController@update')->name('admin.update-metadata');
    Route::post('delete-metadata', "Admin\MetadataCrudController@destroy")->name('admin.delete-metadata');
});

Auth::routes();
